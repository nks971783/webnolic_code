@extends('layouts/master')

@section('banner')

<div class="banner banner-inner tc-light">
				<div class="banner-block">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="banner-content">
									<h1 class="banner-heading">Latest Blog</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="bg-image">
						<img src="{{ asset('images/banner-sm-g.jpg') }}" alt="banner">
					</div>
				</div>
				
</div>

@endsection

@section('content')

<!-- section/blog -->
	<div class="section blog section-x tc-grey">
		<div class="container">

			<div class="row gutter-vr-30px">
				<div class="col-md-8">
					<div class="post post-full">
						<div class="post-thumb">
							<a href="dallas-news-details.html">
								<img src="{{ asset('images/post-thumb-md-a.jpg') }}" alt="banner2">
							</a>
						</div>
						<div class="post-entry d-sm-flex d-block align-items-start">
							<div class="post-date">
								<p>Mar <strong>19</strong></p>
							</div>
							<div class="post-content">
								<div class="post-author d-flex align-items-center">
									<div class="author-thumb">
										<img src="asset('images/author-image-a.jpg') }}" alt="banner3">
									</div>
									<div class="author-name">
										<p>Mark Anthony</p>
									</div>
								</div>
								<h3><a href="dallas-news-details.html">One of the Best UX Agencies in the World</a></h3>
								<div class="content">
									<p>The Demodern team is responsible for the diverse solutions of the individual applications, the overall staging and conception of the 'Discovery Dock. exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
								</div>
								<div class="post-tag d-flex">
									<ul class="post-cat">
										<li><a href="#"><em class="icon ti-bookmark"></em> <span>Design , UI / UX</span></a></li>
									</ul>
									<ul class="post-cat">
										<li><a href="#"><em class="icon ti-comment"></em> <span>2 Comments</span></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div><!-- .post -->
					<div class="post post-full">
						<div class="post-thumb">
							<a href="dallas-news-details.html">
								<img src="{{ asset('images/post-thumb-md-b.jpg') }}" alt="banner4">
							</a>
						</div>
						<div class="post-entry d-sm-flex d-block align-items-start">
							<div class="post-date">
								<p>Mar <strong>15</strong></p>
							</div>
							<div class="post-content">
								<div class="post-author d-flex align-items-center">
									<div class="author-thumb">
										<img src="{{ asset('images/author-image-b.jpg') }}" alt="banner5">
									</div>
									<div class="author-name">
										<p>Maria Helen</p>
									</div>
								</div>
								<h3><a href="dallas-news-details.html">In spring DuMont opens a mixed reality experience for the Port of Hamburg</a></h3>
								<div class="content">
									<p>Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure</p>
								</div>
								<div class="post-tag d-flex">
									<ul class="post-cat">
										<li><a href="#"><em class="icon ti-bookmark"></em><span> Software , Boosting </span> </a></li>
									</ul>
									<ul class="post-cat">
										<li><a href="#"><em class="icon ti-comment"></em> <span>12 Comments</span> </a></li>
									</ul>
								</div>
							</div>
						</div>
					</div><!-- .post -->
					<div class="post post-full">
						<div class="post-thumb">
							<a href="dallas-news-details.html">
								<img src="{{ asset('images/post-thumb-md-c.jpg') }}" alt="banner6">
							</a>
						</div>
						<div class="post-entry d-sm-flex d-block align-items-start">
							<div class="post-date">
								<p>Mar <strong>11</strong></p>
							</div>
							<div class="post-content">
								<div class="post-author d-flex align-items-center">
									<div class="author-thumb">
										<img src="{{ asset('images/author-image-d.jpg') }}" alt="banner7">
									</div>
									<div class="author-name">
										<p>Mark Anthony   </p>
									</div>
								</div>
								<h3><a href="dallas-news-details.html">Los Angeles: DMDRN on retail tour.</a></h3>
								<div class="content">
									<p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences</p>
								</div>
								<div class="post-tag d-flex">
									<ul class="post-cat">
										<li><a href="#"><em class="icon ti-bookmark"></em> <span>Design , UI / UX</span>  </a></li>
									</ul>
									<ul class="post-cat">
										<li><a href="#"><em class="icon ti-comment"></em> <span>12 Comments</span> </a></li>
									</ul>
								</div>
							</div>
						</div>
					</div><!-- .post -->
				</div><!-- .col -->
				<div class="col-md-8 order-md-last">
					<div class="button-area pagination-area">
						<ul class="pagination text-center text-md-right">
							<li class="active"><a href="">1</a></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
						</ul>
					</div>
				</div><!-- .col -->
				<div class="col-md-4 pl-lg-4">
					<div class="sidebar">
						<div class="wgs wgs-sidebar wgs-search">
							<div class="wgs-content">
								<div class="form-group">
									<input type="text" class="form-input"  placeholder="Search...">
									<button class="search-btn"><i class="ti ti-search" aria-hidden="true"></i></button>
								</div>
							</div>
						</div><!-- .wgs -->
						<div class="wgs wgs-sidebar bg-secondary wgs-recents">
							<h3 class="wgs-heading">Recent News</h3>
							<div class="wgs-content">
								<ul class="post-recent">
									<li>
										<h5><a href="dallas-news-details.html">Bringing Great Design Ideas To Completion </a></h5>
										<p class="post-tag">DECEMBER 12, 2018</p>
									</li>
									<li>
										<h5><a href="dallas-news-details.html">Highlights z UX Cambridge 2018 </a></h5>
										<p class="post-tag">DECEMBER 10, 2018</p>
									</li>
									<li>
										<h5><a href="dallas-news-details.html">Ladies, Wine & Design Hamburg #10: "New Tech" panel in our Werkhalle</a></h5>
										<p class="post-tag">DECEMBER 12, 2018</p>
									</li>
								</ul>
							</div>
						</div><!-- .wgs -->
						<div class="wgs wgs-sidebar bg-secondary wgs-category">
							<h3 class="wgs-heading">Categories</h3>
							<div class="wgs-content">
								<ul class="wgs-menu">
									<li><a href="#">Business  <span class="base">(10)</span></a></li>
									<li><a href="#">Technology  <span class="base">(01)</span></a></li>
									<li><a href="#">Development  <span class="base">(17)</span></a></li>
									<li><a href="#">Marketing   <span class="base">(40)</span></a></li>
								</ul>
							</div>
						</div><!-- .wgs -->
						<div class="wgs wgs-sidebar bg-secondary wgs-archive">
							<h3 class="wgs-heading">Archives</h3>
							<div class="wgs-content">
								<ul class="wgs-menu">
									<li><a href="#">December 2017 <span>(10)</span></a></li>
									<li><a href="#">November 2017 <span>(01)</span></a></li>
									<li><a href="#">Octobor 2017 <span>(17)</span></a></li>
									<li><a href="#">September <span>(05)</span></a></li>
									<li><a href="#">August <span>(40)</span></a></li>
								</ul>
							</div>
						</div><!-- .wgs -->
						<div class="wgs wgs-sidebar bg-secondary wgs-tags">
							<h3 class="wgs-heading">Tags</h3>
							<div class="wgs-content">
								<ul class="tag-list">
									<li><a href="">Shipping</a></li>
									<li><a href="">Cargo</a></li>
									<li><a href="">Delivery</a></li>
									<li><a href="">Safe</a></li>
									<li><a href="">Fast</a></li>
									<li><a href="">sea</a></li>
								</ul>
							</div>
						</div><!-- .wgs -->
					</div><!-- .sidebar -->
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- .container -->
	</div>
	<!-- end section/blog -->

@endsection