<html> 
    <head> 
        <title>Contact Information</title> 
    </head> 
    <body> 
        <h1>{{$user->name}} wants to contact with you.</h1> 
        <table cellspacing="10" cellpadding="5" style="border: 2px dashed #FB4314; width: 100%;"> 
            <tr> 
                <th>Name:</th><td>{{$user->name}}</td> 
            </tr> 
            <tr style="background-color: #e0e0e0;"> 
                <th>Email:</th><td>{{$user->email}}</td> 
            </tr>
            <tr> 
                <th>Address:</th><td>{{$user->address}}</td> 
            </tr> 
            <tr style="background-color: #e0e0e0;"> 
                <th>Company:</th><td>{{$user->company}}</td> 
            </tr>
            <tr> 
                <th>Project Start Date:</th><td>{{$user->date}}</td> 
            </tr> 
            <tr style="background-color: #e0e0e0;"> 
                <th>Budget:</th><td>{{$user->budget}}</td> 
            </tr> 
            <tr> 
                <th>Message:</th><td>{{$user->message}}</td> 
            </tr> 
        </table> 
    </body> 
    </html>'; 
