@extends('layouts.admin_app')

@section('content')
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Blog</a></div>
  </div>

  <div class="container-fluid">

  	<div class="row-fluid">
      <div class="span10">
      	
        <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit Blog</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url('admin/update-blog/'.$blog->id) }}" method="post" class="form-horizontal">
            @csrf
            <div class="control-group">
              <label class="control-label">Title :</label>
              <div class="controls">
                <input type="text" class="span11" value="{{ $blog->title }}" name="title" placeholder="Title" />
                @if ($errors->has('title')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('title') }}</span>
                @endif
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Select Category</label>
              <div class="controls">
                <select name="category">
                  @foreach($categories as $category)
                    <option <?php if($category->id == $blog->category_id){ 'selected'; } ?> value="{{ $category->id }}">{{ $category->name }}</option>
                  @endforeach
                </select><br/>
                @if ($errors->has('category')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('category') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Image: </label>
              <div class="controls">
                <input name="image" type="file" />
                <img style="width:50px;height:50px" src="{{Storage::disk('s3')->url($blog->image)}}" /><br/>
                @if ($errors->has('image')) 
                      <span for="required" generated="true" class="help-inline error">{{ $errors->first('image') }}</span>
                @endif
              </div>
            </div>
            

            <div class="control-group">
              <label class="control-label">Content :</label>
                <div class="controls">
                  <textarea id="summernote" name="description" class="textarea_editor span12" rows="6" placeholder="Enter Description ...">{{ $blog->description }}</textarea>
                  @if ($errors->has('description')) 
                    <span for="required" generated="true" class="help-inline error">{{ $errors->first('description') }}</span>
                  @endif
                </div>
            </div>

            <div class="control-group">
              <label class="control-label">Status</label>
              <div class="controls">
                <select name="status">
                    <option <?php if($blog->status == '1'){ echo 'selected'; } ?> value="1">Active</option>
                    <option <?php if($blog->status == '0'){ echo 'selected'; } ?> value="0">Inactive</option>
                </select><br/>
                @if ($errors->has('status')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('status') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Meta Title :</label>
              <div class="controls">
                <input type="text" class="span11" name="meta_title" value="{{ $blog->meta_title }}" placeholder="Meta Title" />
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Meta Keywords :</label>
              <div class="controls">
                <input type="text" class="span11" value="{{ $blog->meta_keyword}}" name="meta_keyword" placeholder="Meta Keyword" />
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Meta Description :</label>
                <div class="controls">
                  <textarea name="meta_description" class="span12" rows="6" placeholder="Enter Description ...">{{ $blog->meta_description}}</textarea>
                </div>
            </div>

            <div class="control-group">
              <label class="control-label">Publish At:</label>
              <div class="controls">
                <div  data-date="2019-09-17" class="input-append date datepicker">
                  <input type="text" name="published_at" value="{{ date('Y-m-d',strtotime($blog->published_at)) }}"  data-date-format="mm-dd-yyyy" class="span11" >
                  <span class="add-on"><i class="icon-th"></i></span> </div>
                  @if ($errors->has('published_at')) 
                    <span for="required" generated="true" class="help-inline error">{{ $errors->first('published_at') }}</span>
                  @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Is Featured?</label>
              <div class="controls">
                <label>
                  <input type="radio" <?php if($blog->featured == '1'){ ?> checked="true" <?php } ?> value="1" name="featured" />
                  Yes</label>
                <label>
                  <input type="radio" <?php if($blog->featured == '0'){ ?> checked="true" <?php } ?> value="0" name="featured" />
                  No</label>
              </div>
            </div>

            <div class="form-actions">
              <button type="submit" class="btn btn-success">Save</button>
            </div>
          </form>
        </div>
      </div>

      </div>
	   </div>

  </div>

</div>
@endsection
