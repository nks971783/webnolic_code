@extends('layouts.admin_app')

@section('content')
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Blog</a></div>
  </div>

  <div class="container-fluid">

  	<div class="row-fluid">
      <div class="span12">
      	@if(session('success'))
	      	<div class="alert alert-success">
	              <button class="close" data-dismiss="alert">×</button>
	              <strong>Success!</strong> {{ session('success') }}
	        </div>
         @endif
      	<a href="{{ url('admin/add-blog') }}" class="btn btn-primary">Add New</a>
      	<div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Blog List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered" id="blogResult">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Thumbnail</th>
                  <th width="300px">Title</th>
                  <th>Category</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              	<?php $i=1; ?>
              	@if(count($blogs) > 0)
              		@foreach($blogs as $blog)
		                <tr id="row_id{{ $blog->id }}" class="gradeX">
		                  <td>{{ $i }}</td>
		                  <td><img style="width:50px;height:50px" src="{{Storage::disk('s3')->url($blog->image)}}" /></td>
		                  <td>{{ $blog->title }}</td>
		                  <td>{{ $blog->category->name }}</td>
		                  <td class="center">
		                  	<a href="{{ url('admin/edit-blog/'.$blog->id) }}" class="btn btn-warning btn-mini">Edit</a>
		                  	<a href="javascript:void(0);" onclick="showPopup('<?php echo url('admin/delete-blog/'.$blog->id); ?>','<?php echo $blog->id; ?>','blogResult')" class="btn btn-danger btn-mini">Delete</a>
		                  </td>
		                </tr>
		                <?php $i++; ?>
	                @endforeach
	            @else

	            	<tr class="gradeX"><td colspan="5">No Result Found!!</td></tr>

	            @endif
              </tbody>
            </table>
            {{ $blogs->links() }} 
          </div>
        </div>

      </div>
	</div>

  </div>

</div>
<style>
.rating {
  unicode-bidi: bidi-override;
  direction: rtl;
}
.rating > span {
  display: inline-block;
  position: relative;
  width: 1.1em;
  cursor: pointer;
}
.rating > span:hover:before,
.rating > span:hover ~ span:before {
    content: "\2605";
    position: absolute;
    left: 0;
    color: gold;
}
.rating > span.active{
  color: gold;
}
.pagination li {
    display: inline-block;
    color: #99412b;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    border: 1px solid #ddd;
}
.page-item.active {
    background-color: #4CAF50;
    color: white;
}
.pagination li:hover:not(.active) {
    background-color: #ddd;
}
</style>
<script>
$( document ).ready(function() {

var base_url = 'http://localhost/blognolic/public/admin'; 

$('.add_views').click(function(){
  var that = this;
  let blogId = $(this).attr('data-id');
  $.ajax({url: base_url+'/add-view/'+blogId, success: function(data){
                //console.log(data);
                if(data.status == '1'){
                    $(that).prev().text(parseInt($(that).prev().text())+1);
                }else{
                    alert('Something Went wrong,Please try later');
                }
  }});
}); 

$('.rating span').click(function(){
  var rating_that = this;
  let blogId = $(this).attr('data-id');
  let star = $(this).attr('data');
  // console.log(blogId, star);
  $.ajax({url: base_url+'/add-review/'+blogId+'/'+star, success: function(data){
                //console.log(data);
                if(data.status == '1'){
                    $(rating_that).parent().prev().text(parseInt($(rating_that).parent().prev().text())+1);
                }else{
                    alert('Something Went wrong,Please try later');
                }
  }});
});

});
</script>
@endsection
