@extends('layouts/master')

@section('banner')

<div class="banner tc-light">
                <div class="banner-block">
                    <div class="container">
                        <div class="row ">
                            <div class="col-md-10 col-xl-7 offset-xl-0">
                                <div class="banner-content">
                                    <h1 class="banner-heading">Make The Shift From Now to Next.</h1>
                                    <p class="lead lead-lg">We merge imagination and technology to help brands grow in an age of digital transformation.</p>
                                    <div class="banner-btn">
                                        <a href="{{ url('portfolio') }}" class="btn">Check Out Our Work</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-image change-bg">
                        <img src="https://webnolic-dev.s3.amazonaws.com/images/1570010658_home-banner.png" alt="home_banner">
                        <!--<video muted="" autoplay="" loop="" preload="auto" poster="{{ asset('images/RHP-Homepage-poster.jpg') }}">
                            <source src="{{ asset('images/RHP-Homepage.webm') }}" type="video/webm">
                            <source src="{{ asset('images/RHP-Homepage.mp4') }}y" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>-->
                    </div>
                </div>
</div>

@endsection

@section('content')


    <!-- section -->
    <div class="section section-x tc-grey-alt">
        <div class="container">
            <div class="row justify-content-between gutter-vr-30px">
                <div class="col-lg-4 col-xl-3 col-md-8">
                    <div class="section-head section-head-col">
                        <h5 class="heading-xs dash">What we do</h5>
                        <h2>Innovations and impact</h2>
                        <p class="lead">Map a strategy, build a solution or elevate your product experience with focused engagements available as standalone offerings or as a part of your project’s service stack.</p>
                        <a href="{{ url('service') }}" class="btn">Discover More</a>
                    </div>
                </div><!-- .col -->
                <div class="col-lg-8">
                    <div class="row gutter-vr-30px gutter-vr-30px-res">
                        <div class="col-sm-6">
                            <div class="feature">
                                <div class="feature-icon">
                                    <img src="https://webnolic-dev.s3.amazonaws.com/images/1570012038_dm-icon.png" width="200" alt="icon">
                                </div>
                                <div class="feature-content">
                                    <h3>Digital Marketing</h3>
                                    <p>Prospects are people too. Marketing should be scientific, but never robotic.</p>
                                    <a href="{{ url('digital-marketing') }}" class="btn btn-arrow">Read More</a>
                                </div>
                            </div>
                        </div><!-- .col -->
                        <div class="col-sm-6">
                            <div class="feature">
                                <div class="feature-icon">
                                    <img src="https://webnolic-dev.s3.amazonaws.com/images/1570012039_wad-icon.png" width="200" alt="icon">
                                </div>
                                <div class="feature-content">
                                    <h3>Website Development</h3>
                                    <p>Productive & engaging web solutions for smarter work and improved customer service.</p>
                                    <a href="{{ url('website-development') }}" class="btn btn-arrow">Read More</a>
                                </div>
                            </div>
                        </div><!-- .col -->
                        <div class="col-sm-6">
                            <div class="feature">
                                <div class="feature-icon">
                                    <img src="https://webnolic-dev.s3.amazonaws.com/images/1570012038_md-icon.png" width="200" alt="icon">
                                </div>
                                <div class="feature-content">
                                    <h3>Mobile Application</h3>
                                    <p>Native, hybrid, and cross-platform mobile apps for consumer-facing and corporate environments.</p>
                                    <a href="{{ url('mobile-application') }}" class="btn btn-arrow">Read More</a>
                                </div>
                            </div>
                        </div><!-- .col -->
                        <div class="col-sm-6">
                            <div class="feature">
                                <div class="feature-icon">
                                    <img src="https://webnolic-dev.s3.amazonaws.com/images/1570012037_cloud.png" width="300" alt="icon">
                                </div>
                                <div class="feature-content">
                                    <h3>Cloud Services</h3>
                                    <p>The cloud offers many incredible advantages, which can definitely create a prolonged change in the web application development lifecycle.</p>
                                    <a href="{{ url('cloud-services') }}" class="btn btn-arrow">Read More</a>
                                </div>
                            </div>
                        </div><!-- .col -->
                        <div class="col-sm-6">
                            <div class="feature">
                                <div class="feature-icon">
                                    <img src="https://webnolic-dev.s3.amazonaws.com/images/1570012038_e-commerce.png" width="300" alt="icon">
                                </div>
                                <div class="feature-content">
                                    <h3>eCommerce</h3>
                                    <p>Increase clarity and wealth of information, which leads to a better user experience and improves conversion rates.</p>
                                    <a href="{{ url('e-commerce') }}" class="btn btn-arrow">Read More</a>
                                </div>
                            </div>
                        </div><!-- .col -->
                        <div class="col-sm-6">
                            <div class="feature">
                                <div class="feature-icon">
                                    <img src="https://webnolic-dev.s3.amazonaws.com/images/1570012035_ai_ml.png" width="300" alt="icon">
                                </div>
                                <div class="feature-content">
                                    <h3>Machine & AI</h3>
                                    <p>Artificial Intelligence is the machines which are designed and programmed in such a manner that they and think and act like a human.</p>
                                    <a href="{{ url('machine-ai') }}" class="btn btn-arrow">Read More</a>
                                </div>
                            </div>
                        </div><!-- .col -->
                    </div><!-- .row -->
                </div>
            </div>
        </div>
    </div>
    <!-- .section -->

    <!-- section -->
    <div class="section section-x section-block tc-light">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-lg-9">
                    <div class="text-block feature-area bg-darker">
                        <div class="section-head">
                            <h5 class="heading-xs dash">Who We are</h5>
                            <h2>Our designers and engineers know collaboration is the essence</h2>
                        </div>
                        <div class="row gutter-vr-40px">
                            <div class="col-sm-6">
                                <div class="feature">
                                    <div class="feature-icon feature-icon-s2">
                                        <em class="icon ti-notepad"></em>
                                    </div>
                                    <div class="feature-content feature-content-s4">
                                        <h4>Quality Products</h4>
                                        <p>We provide quality products with top creative talent build brands that stand out section. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="feature">
                                    <div class="feature-icon feature-icon-s2">
                                        <em class="icon ti-bookmark-alt"></em>
                                    </div>
                                    <div class="feature-content feature-content-s4">
                                        <h4>True Value</h4>
                                        <p>It’s not just our track record the efficiency  We increase true value talent to build.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="feature">
                                    <div class="feature-icon feature-icon-s2">
                                        <em class="icon ti-bag"></em>
                                    </div>
                                    <div class="feature-content feature-content-s4">
                                        <h4>Discover & Sharing</h4>
                                        <p>Whether you are fortune we Discover & share products bonorum undoubtable  creative.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="feature">
                                    <div class="feature-icon feature-icon-s2">
                                        <em class="icon ti-reload"></em>
                                    </div>
                                    <div class="feature-content feature-content-s4">
                                        <h4>Building Faster</h4>
                                        <p>Unlock opportunity by creating discover human centered products  through  build faster.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- .col -->
            </div>
        </div>
        <div class="bg-image bg-fixed">
            <img src="https://webnolic-dev.s3.amazonaws.com/images/1570010926_whatWeAre.jpg" alt="WhatweAre">
        </div>
    </div>
    <!-- .section -->

    <!-- section -->
    <div class="section section-x tc-grey">
        <div class="container">
            <div class="row justify-content-between align-items-center gutter-vr-30px">
                <div class="col-xl-7 col-lg-6">
                    <div class="image-block">
                        <img src="https://webnolic-dev.s3.amazonaws.com/images/1570011281_homeAbout.jpg" alt="homeAbout">
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6">
                    <div class="text-block text-box-ml mtm-15">
                        <h2>We help clients to create digital amazing experience. </h2>
                        <p class="lead">We can help you to unlock opportunity by creating human-centered products.</p>
                        <p>We develop custom websites from the ground up, beautifully designed and tailored specifically to your business needs and challenges, through creativity and using the latest technology.</p>
                        <a href="{{ url('about') }}" class="btn">About us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .section -->

    <!-- section -->
    <div class="section section-x bg-secondary tc-grey-alt">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="section-head section-sm">
                        <h5 class="heading-xs dash dash-both">Our Process</h5>
                        <h2>We make products with strategy</h2>
                    </div>
                </div><!-- .col -->
            </div><!-- .row -->
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6 text-center">
                    <div class="feature feature-s2">
                        <div class="feature-icon-box">
                            <div class="feature-icon feature-icon-s3">
                                <em class="icon ti-clipboard"></em>
                            </div>
                            <div class="feature-heading feature-heading-s2">
                                <h3>Concept</h3>
                            </div>
                        </div>
                        <div class="feature-content-s2">
                            <p>We learn the ins and outs of your business through research and initial meetings. Based on your objectives we tailor recommendations for your online solution.</p>
                        </div>
                    </div>
                </div><!-- .col -->
                <div class="col-lg-4 col-md-6 text-center">
                    <div class="feature feature-s2">
                        <div class="feature-icon-box">
                            <div class="feature-icon feature-icon-s3">
                                <em class="icon ti-layers"></em>
                            </div>
                            <div class="feature-heading feature-heading-s2">
                                <h3>Production</h3>
                            </div>
                        </div>
                        <div class="feature-content-s2">
                            <p>Results you can actually see. The highest standards of design and development give way to a launch you’ll celebrate.</p>
                        </div>
                    </div>
                </div><!-- .col -->
                <div class="col-lg-4 col-md-6 text-center">
                    <div class="feature feature-s2">
                        <div class="feature-icon-box">
                            <div class="feature-icon feature-icon-s3">
                                <em class="icon ti-layout"></em>
                            </div>
                            <div class="feature-heading feature-heading-s2">
                                <h3>Post Production</h3>
                            </div>
                        </div>
                        <div class="feature-content-s2">
                            <p>Through detailed analytics and reporting, see exactly how your website is performing and receive custom recommendations from our team.</p>
                        </div>
                    </div>
                </div><!-- .col -->
            </div><!-- .row-->
        </div>
    </div>
    <!-- .section -->
    
    <!-- section -->
    <div class="section section-x section-project pb-0 tc-grey-alt" id="project">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 text-center">
                    <div class="section-head">
                        <h5 class="heading-xs dash dash-both">Fetured Work</h5>
                        <h2>Our mission is to deliver remarkable experiences and superior results for our clients </h2>
                    </div>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <!-- project filter -->
                    <ul class="project-filter project-md">
                        <li class="active" data-filter="all">All</li>
                        <li data-filter="1">UI/UX</li>
                        <li data-filter="2">Development</li>
                    </ul>
                    <!-- .project-filter -->
                </div>
            </div>
        </div><!-- .container -->
        <div class="project-area bg-secondary">
            <div class="row project project-v5 no-gutters" id="project1">
                <?php $i = 1; ?>
                @foreach($portfolio as $list)
                <div class="col-sm-6 col-xl-3 filtr-item" data-category="<?php if($i%2 == 0){ echo 2; }else{ echo 1; } ?>" style="padding: 10px;">
                    <a href="javascript:void(0);">
                        <div class="project-item">
                            <div class="project-image">
                                <img src="{{ $list->url }}" style="width:300px;height:auto" alt="portfolio">
                            </div>
                            <div class="project-over">
                                <div class="project-content">
                                    <h4>{{ $list->name }}</h4>
                                    <p><?php if($i%2 == 0){ echo 'Development'; }else{ echo 'UI/UX Design'; } ?></p>
                                </div>  
                            </div>
                        </div>
                    </a>
                </div><!-- .col -->
                <?php $i++; ?>
                @endforeach
                
            </div><!-- .row -->
            <div class="project-area project-call section-m bg-dark-s2 tc-light">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="d-lg-flex text-center text-lg-left align-items-center justify-content-between">
                                <h2 class="fw-4">Want to see our more creative work?</h2>
                                <a href="{{ url('portfolio') }}" class="btn btn-sm">See All Work</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .project-area -->
    </div>
    <!-- .section -->

    <!-- section -->
    <!--<div class="section section-x">
        <div class="container">
            <div class="row justify-content-center ">
                <div class="col-lg-8 text-center">
                    <div class="section-head section-md">
                        <h5 class="heading-xs dash dash-both">Testimonial</h5>
                        <h2>What clients say about Genox</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="tes-s1">
                        <div class="has-carousel" data-items="1" data-loop="true" data-dots="true" data-auto="true" data-navs="true">
                            <div class="tes-item">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="tes-thumb">
                                            <div class="bg-image">
                                                <img src="{{ asset('images/clients-a.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="tes-block tc-light bg-primary">
                                            <div class="tes-content">
                                                <p class="tes-title u-cap"><strong>They are great agency</strong> </p>
                                                <p class="lead">I recently hired Genox to develop a new version of my most popular website and I’m extremely happy with their work. </p>
                                            </div>
                                            <div class="author-con">
                                                <h6 class="author-name t-u">Mike Andrew</h6>
                                                <p>CEO  - Philandropia</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tes-item">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="tes-thumb">
                                            <div class="bg-image">
                                                <img src="{{ asset('images/clients-b.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="tes-block tc-light bg-primary">
                                            <div class="tes-content">
                                                <p class="tes-title u-cap"><strong>They are great agency</strong> </p>
                                                <p class="lead">We love working with Genox Not only do they create beautiful, interactive but they are also extremely helpful and amazing.</p>
                                            </div>
                                            <div class="author-con">
                                                <h6 class="author-name t-u">MARINA SHOVA</h6>
                                                <p>CEO  - Art policy</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tes-item">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="tes-thumb">
                                            <div class="bg-image">
                                                <img src="{{ asset('images/clients-c.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="tes-block tc-light bg-primary">
                                            <div class="tes-content">
                                                <p class="tes-title u-cap"><strong>They are great agency</strong> </p>
                                                <p class="lead">These guys are legit. I’ll never hire another agency as long as I’m working. They are also extremely helpful and amazing.</p>
                                            </div>
                                            <div class="author-con">
                                                <h6 class="author-name t-u">HYPER MACK</h6>
                                                <p>CEO  - Hellozen Bulk</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tes-arrow">
                            <a class='slick-prev slick-arrow'><i class='icon ti ti-angle-left'></i></a>
                            <a class='slick-next slick-arrow'><i class='icon ti ti-angle-right'></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <!-- .section -->

    <!-- team -->
    <!--<div class="section section-x team bg-secondary tc-grey">
        <div class="container">
            <div class="row justify-content-center justify-content-sm-start gutter-vr-30px">
                <div class="col-lg-9 col-xl-3 text-center text-sm-left pl-sm-0">
                    <div class="section-head section-head-col box-pad-sm">
                        <h5 class="heading-xs dash">Meet the team</h5>
                        <h2>People of behind work</h2>
                        <p class="lead">We’re always expanding hard working creators.</p>
                        <a href="dallas-team.html" class="btn btn-sm">Meet All Team</a>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-4">
                    <div class="team-single text-center">
                        <div class="team-image">
                            <img src="{{ asset('images/team-a.jpg') }}" alt="">
                        </div>
                        <div class="team-content">
                            <h5 class="team-name">Andrew rover</h5>
                            <p>CEO, Genox</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-4">
                    <div class="team-single text-center">
                        <div class="team-image">
                            <img src="{{ asset('images/team-b.jpg') }}" alt="">
                        </div>
                        <div class="team-content">
                            <h5 class="team-name">Marina Bedi</h5>
                            <p>Developer, Genox</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-4">
                    <div class="team-single text-center">
                        <div class="team-image">
                            <img src="{{ asset('images/team-c.jpg') }}" alt="">
                        </div>
                        <div class="team-content">
                            <h5 class="team-name">Ajax Holder</h5>
                            <p>Head of Research, Genox</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <!-- .team -->

    <!-- logo -->
    <!--<div class="section section-logo">
        <div class="container">
            <div class="row justify-content-center justify-content-md-between gutter-vr-30px">
                <div class="col-sm-3 col-md-2 col-5">
                    <div class="logo-item">
                        <img src="{{ asset('images/client-a.png') }}" alt="">
                    </div>
                </div>
                <div class="col-sm-3 col-md-2 col-5">
                    <div class="logo-item">
                        <img src="{{ asset('images/client-b.png') }}" alt="">
                    </div>
                </div>
                <div class="col-sm-3 col-md-2 col-5">
                    <div class="logo-item">
                        <img src="{{ asset('images/client-c.png') }}" alt="">
                    </div>
                </div>
                <div class="col-sm-3 col-md-2 col-5">
                    <div class="logo-item">
                        <img src="{{ asset('images/client-d.png') }}" alt="">
                    </div>
                </div>
                <div class="col-sm-3 col-md-2 col-5">
                    <div class="logo-item">
                        <img src="{{ asset('images/client-e.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <!-- .logo -->

    <!-- section -->
    <div class="section section-x section-counter tc-light">
        <div class="container">
            <div class="row justify-content-between align-items-center gutter-vr-30px">
                <div class="col-md-5">
                    <div class="text-box mrm-20">
                        <h2>Our top minds make it happen with digital  approach</h2>
                        <p>Our award-winning design team and engineers with proficiency across multiple fields & specializations.</p>
                    </div>
                </div><!-- .col -->
                <div class="col-md-6">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <div class="counter counter-s2 counter-bdr border-bottom-0">
                                <div class="counter-icon">
                                    <em class="icon ti-dropbox"></em>
                                </div>
                                <div class="counter-content counter-content-s2">
                                    <h2 class="count" data-count="34">34</h2>
                                    <p>Brands Helped</p>
                                </div>
                            </div>
                        </div><!-- .col -->
                        <div class="col-6">
                            <div class="counter counter-s2 counter-bdr border-bottom-0">
                                <div class="counter-icon">
                                    <em class="icon ti-basketball"></em>
                                </div>
                                <div class="counter-content counter-content-s2">
                                    <h2 class="count" data-count="145">+145</h2>
                                    <p>Ongoing Task</p>
                                </div>
                            </div>
                        </div><!-- .col -->
                        <div class="col-6">
                            <div class="counter counter-s2 counter-bdr">
                                <div class="counter-icon">
                                    <em class="icon ti-pencil-alt"></em>
                                </div>
                                <div class="counter-content counter-content-s2">
                                    <h2 class="count" data-count="437">+437</h2>
                                    <p>Projects Done</p>
                                </div>
                            </div>
                        </div><!-- .col -->
                        <div class="col-6">
                            <div class="counter counter-s2 counter-bdr">
                                <div class="counter-icon">
                                    <em class="icon ti-user"></em>
                                </div>
                                <div class="counter-content counter-content-s2">
                                    <h2 class="mrm-4 count" data-count="375">+375</h2>
                                    <p>Satisfied Clients</p>
                                </div>
                            </div>
                        </div><!-- .col -->
                    </div>
                </div>
            </div>
        </div>
        <!-- bg -->
        <div class="bg-image bg-fixed">
            <img src="https://webnolic-dev.s3.amazonaws.com/images/1570023204_bg-b.jpg" alt="clients">
        </div>
    </div>
    <!-- .section -->
    
    <!-- section-news -->
    <!--<div class="section section-x section-news">
        <div class="container">
            <div class="row justify-content-center gutter-vr-60px">
                <div class="col-lg-6">
                    <div class="row justify-content-center justify-content-md-start no-gutter">
                        <div class="col-10 col-lg-7 text-center text-md-left">
                            <div class="section-head section-sm">
                                <h5 class="heading-xs dash">News</h5>
                                <h2>From the Genox news room</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center justify-content-md-start gutter-vr-30px">
                        <div class="col-md-12">
                            <div class="post post-v1 align-items-center">
                                <div class="post-thumb">
                                    <a href="dallas-news-details.html"><img src="images/post-a.jpg" alt="post"></a>
                                </div>
                                <div class="post-content text-center text-md-left">
                                    <p class="post-tag post-date">DECEMBER 12, 2018</p>
                                    <h4><a href="dallas-news-details.html">One of the Best UX Agencies in the World</a></h4>
                                    <a href="dallas-dallas-news-details.html" class="btn btn-arrow">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="post post-v1 align-items-center">
                                <div class="post-thumb">
                                    <a href="dallas-news-details.html"><img src="images/post-b.jpg" alt="post"></a>
                                </div>
                                <div class="post-content text-center text-md-left">
                                    <p class="post-tag post-date">DECEMBER 12, 2018</p>
                                    <h4><a href="dallas-news-details.html">5 Tricks To Help Boost Creativity in One Touch</a></h4>
                                    <a href="dallas-news-details.html" class="btn btn-arrow">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center text-md-left">
                            <div class="text-box">
                                <a href="dallas-news.html" class="btn btn-post">View all blog</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="block bdr feed ml-lg-4">
                        <div class="feed-block d-flex">
                            <div class="feed-icon">
                                <i class="icon fab fa-twitter"></i>
                            </div>
                            <div class="feed-content">
                                <h4>Veon Studio</h4>
                                <a href="#"> @ veon_studio</a>
                            </div>
                        </div>
                        <div class="feed-block" id="tweets_feed"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <!-- .section-news -->

@endsection