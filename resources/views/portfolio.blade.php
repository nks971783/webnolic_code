@extends('layouts/master')

@section('banner')
<div class="banner banner-inner tc-light">
				<div class="banner-block">
					<div class="container">
						<div class="row">
							<div class="col-xl-9">
								<div class="banner-content">
									<h1 class="banner-heading">We help Clients Create Joyful Digital Experiences</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="bg-image">
						<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612396_portfolio.jpg" alt="banner">
					</div>
				</div>
				
			</div>
@endsection

@section('content')

<div class="section section-x section-project" id="work">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-8 text-center text-md-left">
					<div class="section-head mtm-10">
						<h2>Some highlights of work we've done for forward thinking clients.</h2>
					</div>
				</div><!-- .col -->
				<div class="col-md-12">
					<!-- project-filter -->
					<ul class="project-filter filter-left project-md text-center text-sm-left">
						<li class="active" data-filter="all">All</li>
						<li data-filter="1">UI/UX</li>
						<li data-filter="2">Development</li>
					</ul>
					<!-- .project-filter -->
				</div>
			</div>
		</div>
		<!-- project -->
		<div class="project-area">
			<div class="container">
				<div class="row project project-s2 gutter-vr-30px" id="project1">
					<?php $i = 1; ?>
                	@foreach($portfolio as $list)
					<div class="col-md-4 col-sm-6 filtr-item" data-category="<?php if($i%2 == 0){ echo 2; }else{ echo 1; } ?>">
						<a href="javascript:void(0);">
							<div class="project-item">
								<div class="project-image">
									<img src="{{ $list->url }}" alt="portfolio-img">
								</div>
								<div class="project-over">
									<div class="project-content">
										<h4>{{ $list->name }}</h4>
                                    	<p><?php if($i%2 == 0){ echo 'Development'; }else{ echo 'UI/UX Design'; } ?></p>
									</div>	
								</div>
							</div>
						</a>
					</div>
					<?php $i++; ?>
                	@endforeach
				</div>
			</div>
		</div>
	</div>

@endsection