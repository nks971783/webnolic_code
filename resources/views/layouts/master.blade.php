<!DOCTYPE html>
<html lang="zxx" class="js">
<head>
	<meta charset="utf-8">
	<meta name="author" content="Webnolic">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!--<meta name="description" content="Webnolic. is your reliable partner that specializes in innovating excellent web apps, mobile apps, eCommerce, cloud, digital marketing & machine learning technologies to businesses across the world.">
   
   <meta property="og:title" content="Webnolic Technologies" />
   <meta name="twitter:description" content="Software Development Services Company | Technology Consulting Company | Software Outsourcing Company | ERP/SAAS Applications | Mobile Applications | Google Maps Customization Company" />
   <meta name="generator" content="WordPress 4.8.10" />
   <meta name="Development" content="Best software Development compnay in Noida |  Software Outsourcing Company |Mobile Applications| Best Resources" />
   <meta name="keywords" content="Best software Development compnay in Noida |  Software Outsourcing Company |Mobile Applications| Best Resources" />
   <meta name="Technologies" content="PHP | Laravel | Python | Nodejs | Angular | Design Pattern |Database Design | Solid principal | UI | SEO | SMO |digital marketing" />
   <meta name="About" content="We are enthusiastic team providing our best to our clients." />-->
   <meta name="keywords" content="HTML,CSS,XML,JavaScript">
   <meta property="og:url" content="https://webnolic.com/" />
   <meta property="og:type" content="article" />
   <meta name="description" content="Webnolic specializes in innovating web apps, mobile apps, eCommerce, cloud, digital marketing & machine learning technologies.">
	<meta property="og:title" content="Webnolic || Thinking | Wrapping | Creation" />
	<meta property="og:description" content="Webnolic specializes in innovating web apps, mobile apps, eCommerce, cloud, digital marketing & machine learning technologies.">
   <meta property="og:image" content="https://webnolic-dev.s3.amazonaws.com/images/1570010658_home-banner.png" />
   
	<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">
	<!-- Site Title  -->
	<title>Webnolic || Thinking | Wrapping | Creation</title>
	<!-- Bundle and Base CSS -->
	<script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>
		{
			"@context":"https://schema.org",
			"@graph":[
						{
							"@type":"WebSite",
							"@id":"https://webnolic.com/#website",
							"url":"https://webnolic.com/",
							"name":"Webnolic",
							"potentialAction":{"@type":"SearchAction","target":"https://webnolic.com/?s={search_term_string}","query-input":"required name=search_term_string"}
						},
						{
							"@type":"CollectionPage",
							"@id":"https://webnolic.com/#webpage",
							"url":"https://webnolic.com/",
							"inLanguage":"en-US",
							"name":"Webnolic || Thinking | Wrapping | Creation",
							"isPartOf":{"@id":"https://webnolic.com/#website"},
							"description":"Webnolic specializes in innovating web apps, mobile apps, eCommerce, cloud, digital marketing & machine learning technologies."
						}
					]
		}
	</script>
	
	<style>

	@media screen and (min-width: 992px) {

		:root{--blue:#007bff;--indigo:#6610f2;--purple:#6f42c1;--pink:#e83e8c;--red:#dc3545;--orange:#fd7e14;--yellow:#ffc107;--green:#28a745;--teal:#20c997;--cyan:#17a2b8;--white:#fff;--gray:#6c757d;--gray-dark:#343a40;--primary:#007bff;--secondary:#6c757d;--success:#28a745;--info:#17a2b8;--warning:#ffc107;--danger:#dc3545;--light:#f8f9fa;--dark:#343a40;--breakpoint-xs:0;--breakpoint-sm:576px;--breakpoint-md:768px;--breakpoint-lg:992px;--breakpoint-xl:1200px;--font-family-sans-serif:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";--font-family-monospace:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}*,::after,::before{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-webkit-tap-highlight-color:transparent}article,aside,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}h1,h2,h3,h4,h5,h6{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}dl,ol,ul{margin-top:0;margin-bottom:1rem}b,strong{font-weight:bolder}a{color:#007bff;text-decoration:none;background-color:transparent}a:hover{color:#0056b3;text-decoration:underline}img{vertical-align:middle;border-style:none}button{border-radius:0}button,input,optgroup,select,textarea{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button,input{overflow:visible}button,select{text-transform:none}[type=button],[type=reset],[type=submit],button{-webkit-appearance:button}[type=button]:not(:disabled),[type=reset]:not(:disabled),[type=submit]:not(:disabled),button:not(:disabled){cursor:pointer}.h1,.h2,.h3,.h4,.h5,.h6,h1,h2,h3,h4,h5,h6{margin-bottom:.5rem;font-weight:500;line-height:1.2}.h1,h1{font-size:2.5rem}.h2,h2{font-size:2rem}.h3,h3{font-size:1.75rem}.h4,h4{font-size:1.5rem}.h5,h5{font-size:1.25rem}.lead{font-size:1.25rem;font-weight:300}.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.container{max-width:540px}.container{max-width:720px}.container{max-width:960px}.container{max-width:1140px}.row{display:flex;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.no-gutters{margin-right:0;margin-left:0}.no-gutters>.col,.no-gutters>[class*=col-]{padding-right:0;padding-left:0}.col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{position:relative;width:100%;padding-right:15px;padding-left:15px}.col-6{flex:0 0 50%;max-width:50%}.col-12{flex:0 0 100%;max-width:100%}.col-sm-6{flex:0 0 50%;max-width:50%}.col-md-5{flex:0 0 41.6666666667%;max-width:41.6666666667%}.col-md-6{flex:0 0 50%;max-width:50%}.col-md-8{flex:0 0 66.6666666667%;max-width:66.6666666667%}.col-md-10{flex:0 0 83.3333333333%;max-width:83.3333333333%}.col-md-12{flex:0 0 100%;max-width:100%}.col-lg-3{flex:0 0 25%;max-width:25%}.col-lg-4{flex:0 0 33.3333333333%;max-width:33.3333333333%}.col-lg-6{flex:0 0 50%;max-width:50%}.col-lg-8{flex:0 0 66.6666666667%;max-width:66.6666666667%}.col-lg-9{flex:0 0 75%;max-width:75%}.col-lg-12{flex:0 0 100%;max-width:100%}.col-xl-3{flex:0 0 25%;max-width:25%}.col-xl-5{flex:0 0 41.6666666667%;max-width:41.6666666667%}.col-xl-7{flex:0 0 58.3333333333%;max-width:58.3333333333%}.offset-xl-0{margin-left:0}.btn{display:inline-block;font-weight:400;color:#212529;text-align:center;vertical-align:middle;user-select:none;background-color:transparent;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out}.btn-group-sm>.btn,.btn-sm{padding:.25rem .5rem;font-size:.875rem;line-height:1.5;border-radius:.2rem}.nav-link{display:block;padding:.5rem 1rem}.nav-link:focus,.nav-link:hover{text-decoration:none}.bg-primary{background-color:#007bff!important}.bg-secondary{background-color:#6c757d!important}.border-bottom-0{border-bottom:0!important}.d-none{display:none!important}.d-lg-flex{display:flex!important}.justify-content-end{justify-content:flex-end!important}.justify-content-center{justify-content:center!important}.justify-content-between{justify-content:space-between!important}.align-items-center{align-items:center!important}.mr-2,.mx-2{margin-right:.5rem!important}.pb-0,.py-0{padding-bottom:0!important}.text-center{text-align:center!important}.text-lg-left{text-align:left!important}.text-lg-right{text-align:right!important}.far{font-family:'Font Awesome 5 Free';font-weight:400}.fa,.fab,.fal,.far,.fas{-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased;display:inline-block;font-style:normal;font-variant:normal;text-rendering:auto;line-height:1}.fa-paper-plane:before{content:"\f1d8"}[class*=" ti-"],[class^=ti-]{font-family:themify;speak:none;font-style:normal;font-weight:400;font-variant:normal;text-transform:none;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.ti-user:before{content:"\e602"}.ti-search:before{content:"\e610"}.ti-bookmark-alt:before{content:"\e618"}.ti-reload:before{content:"\e619"}.ti-pencil-alt:before{content:"\e61d"}.ti-layout:before{content:"\e62e"}.ti-layers:before{content:"\e62f"}.ti-bag:before{content:"\e655"}.ti-notepad:before{content:"\e67c"}.ti-clipboard:before{content:"\e6b4"}.ti-basketball:before{content:"\e6b9"}.ti-twitter:before{content:"\e72a"}.ti-pinterest:before{content:"\e731"}.ti-facebook:before{content:"\e741"}.ti-dropbox:before{content:"\e742"}body,html{font-family:Roboto,sans-serif;color:#131313;font-size:16px;line-height:1.6;font-weight:400;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;position:relative;overflow-x:hidden;min-width:320px}body,html{font-size:16px;line-height:1.6;font-weight:400}article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section{display:block}ol,ul{list-style:none}.h1,.h2,.h3,.h4,.h5,.h6,h1,h2,h3,h4,h5,h6{color:#131313;font-weight:700;font-family:Poppins,sans-serif;line-height:1.3;margin-bottom:.5rem}.h1:last-child,.h2:last-child,.h3:last-child,.h4:last-child,.h5:last-child,.h6:last-child,h1:last-child,h2:last-child,h3:last-child,h4:last-child,h5:last-child,h6:last-child{margin-bottom:0}.tc-light .h1,.tc-light .h2,.tc-light .h3,.tc-light .h4,.tc-light .h5,.tc-light .h6,.tc-light h1,.tc-light h2,.tc-light h3,.tc-light h4,.tc-light h5,.tc-light h6{color:#fff}p{margin-bottom:1rem}p:last-child{margin-bottom:0}ol,table,ul{margin:0;padding:0}b,strong{font-weight:700}a,button{outline:0 none;transition:all .5s;color:#ff4157}a:link,a:visited,button:link,button:visited{text-decoration:none}a:active,a:focus,a:hover,button:active,button:focus,button:hover{outline:0;color:#ff4157}img{max-width:100%;height:auto}.fz-1,.h1,h1{font-size:1.375rem}.fz-2,.h2,h2{font-size:1.375rem}.fz-3,.h3,h3{font-size:1.125rem}.fz-4,.h4,h4{font-size:1.125rem;line-height:1.667}.fz-5,.h5,h5{font-size:1rem}.fz-1,.h1,h1{font-size:1.75rem}.fz-2,.h2,h2{font-size:1.375rem}.fz-3,.h3,h3{font-size:1.25rem}.fz-4,.h4,h4{font-size:1.125rem;line-height:1.667}.fz-5,.h5,h5{font-size:1rem}.lead{font-weight:400}.fz-2,.h2,h2{font-size:1.875rem;line-height:1.533}.fz-3,.h3,h3{font-size:1.5rem}.fz-4,.h4,h4{font-size:1.125rem;line-height:1.333}.fz-5,.h5,h5{font-size:.875rem}.fz-1,.h1,h1{font-size:2.25rem}.has-bg,.has-bg-image,.has-bg-video,.has-ovm,.nk-df-rel,[class*=bg-]{position:relative}.has-bg-image>[class*=container],.has-bg-video>[class*=container],.has-ovm>:not(.nk-ovm):not(.header-main):not(.nk-header),.has-ovm>[class*=container],.nk-df-over-up{position:relative;z-index:5}.bg-image,.bg-video,.bg-video-cover,.nk-df-around,.overlay:after{position:absolute;bottom:0;top:0;left:0;right:0}.bg-image,.bg-video,.bg-video-cover,.has-bg,.nk-df-bg,[class*=bg-]{background-repeat:no-repeat;background-size:cover;background-position:50% 50%}.gutter-vr-30px{margin-top:-15px!important;margin-bottom:-15px!important}.gutter-vr-30px>div,.gutter-vr-30px>li{padding-top:15px!important;padding-bottom:15px!important}.gutter-vr-40px{margin-top:-20px!important;margin-bottom:-20px!important}.gutter-vr-40px>div,.gutter-vr-40px>li{padding-top:20px!important;padding-bottom:20px!important}.gutter-vr-30px-res{margin-top:-15px!important;margin-bottom:-15px!important}.gutter-vr-30px-res>div,.gutter-vr-30px-res>li{padding-top:15px!important;padding-bottom:15px!important}.gutter-vr-30px{margin-top:-15px!important;margin-bottom:-15px!important}.gutter-vr-30px>div,.gutter-vr-30px>li{padding-top:15px!important;padding-bottom:15px!important}.gutter-vr-40px{margin-top:-20px!important;margin-bottom:-20px!important}.gutter-vr-40px>div,.gutter-vr-40px>li{padding-top:20px!important;padding-bottom:20px!important}.fw-4{font-weight:400!important}.has-bg-image>div,.has-bg-video>div{position:relative;z-index:1}.bg-fixed{background-attachment:fixed}.bg-image{position:absolute!important;z-index:0!important;opacity:0;transition:opacity .4s}.bg-image.bg-image-loaded{opacity:1}.bg-image img{display:none!important}[class*=overlay-],[class*=overlay]{position:relative}[class*=overlay-]::after,[class*=overlay]::after{width:100%;height:100%;position:absolute;content:'';left:0;top:0}.tc-light p{color:#fff!important}.tc-light h1,.tc-light h2,.tc-light h3,.tc-light h4,.tc-light h5,.tc-light h6{color:#fff!important}.tc-grey{color:#4b4b4b!important}.tc-grey-alt{color:#4a4a4a!important}.bg-primary{background-color:#ff4157!important}.bg-darker{background-color:rgba(28,30,41,.9)}.bg-secondary{background-color:#ecf6fa!important}.bg-dark-s2{background:#1c1e29}.bg-dark-alt{background:#0a1015}.lead{font-size:1.25rem;font-weight:300;line-height:1.6}.tc-light{color:#fff}.lead{font-size:1.25rem;line-height:1.5}.lead{font-size:1.5rem}.pb-0{padding-bottom:0}.mr-2{margin-right:2px}.mrm-4{margin-right:-4px}.mrm-20{margin-right:-20px}.mtm-15{margin-top:-15px!important}.btn{position:relative;font-size:14px;font-weight:700;font-family:Poppins,sans-serif;color:#fff;background:#ff4157;text-transform:uppercase;border-radius:0;display:inline-block;padding:10px 20px;transition:all ease .5s;outline-style:none;min-width:120px}.bg-primary .btn{background:#fff;color:#ff4157}.btn-sm{padding:10px 20px}.btn-arrow{font-size:14px;color:#ff4157;font-weight:600;background:0 0;border-color:#e5e5e5;text-transform:capitalize;border:none;position:relative;min-width:auto;padding:0}.btn-arrow::after{position:static;content:"\e661";font-family:themify;font-size:11px;transition:all ease-in .3s;margin-left:6px;display:inline-block;transform:translateX(0)}.btn{padding:13px 30px 14px}.btn-sm{padding:12px 25px}.btn-arrow{padding:0}.text-box{line-height:.8}.text-box h2{color:#131313;margin-bottom:10px}.text-box p:not(.lead){line-height:1.625}.text-box p:last-child{margin-bottom:0}.text-block h2{margin-bottom:13px}.text-block h2+.lead{margin-top:13px;margin-bottom:0}.text-block h2+p{margin-top:-7px}.text-block p+p{margin-top:10px;margin-bottom:0}.text-block p{margin-bottom:0}.text-block p+.btn{margin-top:23px}.text-box-ml{margin-left:15px}.text-block h2+.lead{margin-top:13px;margin-bottom:0}.text-block p+p{margin-top:10px;margin-bottom:0}.text-block p+.btn{margin-top:23px}.heading-xs{font-size:14px;font-weight:500;font-family:Poppins,sans-serif;color:#131313;text-transform:uppercase;margin-bottom:13px;letter-spacing:1.4px;line-height:.8}.tc-grey-alt .heading-xs,.tc-light-dark .heading-xs{color:inherit}.tc-grey-alt .heading-xs{color:#4b4b4b}.bg-darker .heading-xs,.tc-light .heading-xs{color:rgba(255,255,255,.7)!important}.dash{padding-left:35px;padding-right:35px;position:relative;display:inline-block}.dash::after,.dash::before{position:absolute;content:'';width:20px;height:2px;background:#ff4157;left:0;top:0;bottom:0;margin:auto 0}.dash::after{left:auto;right:0}.dash-both{padding-right:35px;padding-left:35px}.dash-both::after{left:auto;right:0}.dash::after{left:0;right:auto}.dash-both::after{left:auto;right:0}.section-head{margin-bottom:38px;line-height:.8}.section-head .heading-xs{margin-bottom:13px}.section-head h2{margin-bottom:15px}.section-head p+a{margin-top:10px}.section-head p{line-height:1.6}.section-head-col{padding-bottom:0;margin-bottom:15px}.section-head-col h2{margin-bottom:0}.section-head-col h2+p{padding-top:11px}.section-head-col p+.btn{margin-top:4px}.section-head{margin-bottom:30px}.section-head p+a{margin-top:10px}.section-head-col{margin-bottom:0}.section-head-col{margin-bottom:33px}.section-sm{margin-bottom:50px}.preloader{position:fixed!important;top:0;left:0;right:0;bottom:0;z-index:10000}.icon{color:#ff4157}.header-box{position:relative}.header-main{padding:12px 0;transition:all .4s;position:absolute;width:100%;z-index:9;left:0}.header-s1 .header-main{background:#fff}.is-boxed .header-main{padding:12px 15px;z-index:9}.header-wrap{position:relative;width:100%;display:flex;justify-content:space-between;align-items:center;flex-wrap:wrap}.header-logo{margin:6px 0;flex-shrink:0}.header-navbar{position:fixed;left:-280px;top:0;width:260px;height:100vh}.header-navbar-overlay,.header-navbar::before{position:absolute;height:100%;width:100%;content:'';top:0}.header-navbar::before{background:#fff;left:50%;transform:translateX(-50%);transition:all .4s}.header-navbar-overlay{background:rgba(18,27,34,.7);left:100%;width:0;z-index:-1;opacity:0;visibility:hidden;transition:opacity .3s;transition-delay:.2s}.header-menu{justify-content:flex-end;padding:30px 25px}.banner{min-height:72px}.is-boxed .banner{padding-left:15px;padding-right:15px}.header-navbar:not(.header-navbar-classic){left:-310px;width:290px}.header-navbar:not(.header-navbar-classic){left:-360px;width:340px}.is-boxed .header-main{padding:12px 40px}.is-boxed .banner{padding-left:40px;padding-right:40px}.logo img{height:36px;transition:all .4s}.header-navbar:not(.header-navbar-classic){height:auto;width:auto;left:0}.header-main{padding:20px 0}.is-boxed .header-main{padding:12px 40px}.header-nav-toggle{display:none}.is-boxed .header-box .banner{padding:0 40px}.logo img{height:40px}.logo img{height:60px}.header-menu{max-height:100vh;overflow:hidden;overflow-y:auto}.header-nav-toggle{height:44px}.header-main{padding:20px 0}.is-boxed .header-main{padding:20px 40px}.is-boxed .header-box .banner{padding:0 40px}.header-navbar-overlay{display:none}.menu{padding:12px 0}.menu-btns{margin-bottom:26px;margin-top:-10px;padding-top:10px;display:flex;position:relative}.menu-btns::before{position:absolute;content:'';left:0;bottom:0;width:1px;height:18px;background:#e5e5e5;margin:auto 0;top:0;display:none}.menu-btns>li{padding:0}.menu-btns>li a{padding:12px 15px}.menu-item{position:relative;border-bottom:1px solid rgba(219,228,247,.75)}.menu-item:last-child{border-bottom:0}.menu-item a{font-family:Poppins,sans-serif;font-size:14px;line-height:16.8px;color:#131313;padding:12px 0;display:block;position:relative;font-weight:600}.menu-item a.active,.menu-item a:hover{color:#ff4157}.menu-item>a.active{color:#ff4157}.menu-btns::before{display:block}.menu-btns>li a{padding:12px 22px}.header-menu{display:flex!important;width:auto;align-items:center;position:static;background:0 0;padding:0;margin:0;border-radius:0;overflow:visible}.header-navbar{padding:0 0 0 15px;margin-top:0;position:relative}.header-navbar:before{display:none}.menu{display:flex;align-items:center;padding:0}.menu-item{border-bottom:none}.menu-item>a{padding:20px 10px;text-transform:uppercase}.menu-item:hover{z-index:9999}.menu-btns{display:flex;margin-bottom:-10px;margin-left:12px;padding-left:15px;padding-top:0}.menu-btns>li{padding:10px}.menu-btns>li:last-child{padding-right:0}.menu-item>a{padding:30px 18px}.section{position:relative;width:100%;padding-top:45px;padding-bottom:45px}.section-m{padding-top:60px;padding-bottom:60px}.section-x{padding-top:75px;padding-bottom:75px}.section{padding-top:51.4285714286px;padding-bottom:51.4285714286px}.section-m{padding-top:34.2857142857px;padding-bottom:34.2857142857px}.section-x{padding-top:85.7142857143px;padding-bottom:85.7142857143px}.section{padding-top:60px;padding-bottom:60px}.section-m{padding-top:40px;padding-bottom:40px}.section-x{padding-top:100px;padding-bottom:100px}.section{padding-top:90px;padding-bottom:90px}.section-m{padding-top:60px;padding-bottom:60px}.section-x{padding-top:150px;padding-bottom:150px}.wgs{margin-bottom:30px}.wgs-content p{color:#4a4a4a;font-size:.875rem}.wgs-logo{max-width:170px}.wgs-logo img{height:36px}.wgs-title{color:#0a1015;font-size:1.125rem;margin-bottom:15px}.wgs-title+form{padding-top:10px}.wgs-menu li a{color:#4a4a4a;display:inline-block;padding:6px 0}.bg-dark-alt .wgs-menu li a{color:#fff}.wgs-menu li:last-child a{padding-bottom:0}.field-group{position:relative}.field-group.btn-inline .button{position:absolute;right:0;top:12px}.field-group .input{width:100%;padding:15px 55px 15px 22px;border:1px solid #e5e5e5}.bg-dark .field-group .input,.bg-dark-alt .field-group .input,.bg-secondary .field-group .input{background:0 0}.field-group .button{background:0 0;font-size:18px;border:none;border-left:1px solid #e5e5e5;padding:7px 16px;color:#ff4157}.footer .wgs{margin-bottom:0}.footer-s1 .field-group .input{background:0 0;border-color:rgba(255,255,255,.4);color:#fff}.footer-s1 .field-group button{border-color:rgba(255,255,255,.4);color:rgba(255,255,255,.6)}.wgs{margin-bottom:20px}.wgs-title{margin-bottom:20px}.wgs-logo img{height:auto}.banner-heading{font-size:1.714rem;font-weight:900;line-height:1.1}.banner{position:relative}.banner-heading+p{margin:25px 0}.banner-block{display:flex;align-items:center;min-height:100vh;padding:70px 0 0}.header-s1 .banner-block{padding:75px 0 0}.banner-content{padding-left:15px;padding-top:50px;padding-bottom:50px}.banner-heading{font-size:2.5rem}.banner-content{padding-left:50px}.banner-heading{font-size:3.75rem;line-height:1.2}.banner-content{padding-left:70px}.header-s1 .banner-block{padding:120px 0 0}.banner-content{padding-left:0}.banner-heading+p{margin:35px 0 55px}.banner-btn{position:relative}.feature{display:flex;height:100%;max-width:400px}.text-center .feature{margin:0 auto}.feature-icon{font-size:48px;color:#ff4157;line-height:.612}.feature-icon-box{position:relative}.feature-icon-s2{font-size:35px}.feature-content{margin-left:20px}.feature-content-s2{font-weight:400}.feature-content-s4{margin-left:20px;margin-top:-6px}.feature-content-s4 p:last-child{margin-bottom:0}.feature-s2{display:block;padding:20px 0}.feature-s2 .icon{font-size:72px;color:#ecf3f6;z-index:-1}.feature-heading{position:absolute;left:0;top:50%;right:0;transform:translateY(-50%)}.feature-heading-s2{position:relative;transform:translateY(0)}.feature-heading-s2 h3{margin:20px 0 10px}.feature-icon-s3{width:70px;height:70px;text-align:center;background:#fff;color:#ff4157;border-radius:50%;margin:0 auto;padding:13px 0}.feature-icon-s3 .icon{font-size:24px;color:#ff4157}.feature-area{padding:45px 35px}.feature{display:block}.feature-content{margin-left:0}.feature-content-s4{margin-top:0}.feature-icon{margin-bottom:20px}.feature-icon-s3{margin-bottom:25px}.feature h4{margin-bottom:11px}.feature-area{padding:50px 40px}.feature-content{margin-left:0}.feature-area{padding:60px 50px}.feature{display:flex}.feature h3{margin-bottom:12px;line-height:1.25}.feature p{margin-bottom:15px}.feature-icon{font-size:49px}.feature-icon-s2{font-size:35px}.feature-content{margin-left:25px}.feature-content-s2 p{margin-bottom:0}.feature-content-s4{margin-left:20px;margin-top:-6px}.feature-s2{display:block;padding:20px 15px 0}.feature-area{padding:80px}.feature-content{margin-left:30px}.feature-content-s4{margin-left:20px}.feature-s2{padding:20px 20px 0}.counter{display:block;height:100%;justify-content:center;text-align:center}.counter-icon{font-size:25px;color:#ff4157;line-height:1.4}.counter-s2{margin:-.5px}.counter-content{margin-left:0}.counter-bdr{border:1px solid #6450b6}.counter h2{margin-bottom:0}.counter p{font-size:.75rem;line-height:1.1}.counter-s2{padding:10px 15px}.counter-s2 .counter-content{margin-left:20px}.counter-icon{font-size:35px}.counter-content{margin-left:0}.counter-s2{padding:30px 40px}.counter p{font-size:.875rem}.counter-icon{font-size:35px}.counter-s2{padding:30px 20px}.counter-s2 .counter-content-s2{margin-left:0}.counter-content{margin-left:0}.counter{display:flex}.counter-content{text-align:left;margin-left:15px}.counter h2{font-size:1.875rem}.counter-icon{font-size:49px}.counter-content{margin-left:25px}.counter-s2 .counter-content-s2{margin-left:20px}.counter-content{margin-left:30px}.section-project{width:100%;overflow:hidden}.project[class*=gutter]{margin-left:0!important;margin-right:0!important}.project-filter{padding:0 0 30px;text-align:center}.project-filter li{display:inline-block;cursor:pointer;padding:0 15px;position:relative;line-height:2.2;color:#4b4b4b;font-weight:300;font-size:1.125rem}.project-filter li:not(:last-child)::after{position:absolute;content:"/";right:-6px;top:0;bottom:0;margin:auto 0;color:#4b4b4b}.project-filter li.active{color:#ff4157!important}.project-call h2{margin-bottom:20px;margin-top:-12px}.project-filter li{padding:0 15px}.project-md{padding-bottom:50px}.project-filter{padding:20px 0 30px}.project-filter li{padding:0 25px}.project-md{padding-bottom:50px}.project-call h2{margin-bottom:0;margin-top:0}.project-filter li{padding:0 32px}.filtr-item a{display:block}.project-item{position:relative;text-align:center}.project-item h4{font-size:1.125rem;color:#ff4157}.project-item p{font-size:.875rem;color:#4b4b4b}.project-image{width:100%;overflow:hidden}.project-image img{width:100%;transition:.5s}.project-content{opacity:1}.project-over{position:absolute;left:15px;top:15px;right:15px;bottom:15px;background:rgba(255,255,255,.9);display:flex;align-items:center;justify-content:center;width:calc(100% - 30px);transition:all .3s;opacity:0;transform:translateY(30px)}.project-v5 [class*=col-]{padding:0}.cta-text h2{font-weight:300;font-size:30px}.cta-btn .btn{padding:14px 34px 13px}.cta-text h2{font-size:2.25rem;line-height:50px}.cta-btn .btn{padding:18px 40px 17px}.cta-btn .btn{padding:24px 54px 23px}form .input{width:100%;font-size:1rem;border:1px solid #ecf6fa;padding:18px 20px;margin-bottom:10px;color:#4b4b4b;background:0 0}form .input::placeholder{color:#4b4b4b;opacity:.7}form .input::-webkit-input-placeholder{color:#4b4b4b;opacity:.7}.bg-dark-alt form .input{color:#a2a9bf}.bg-dark-alt form .input::placeholder{color:#a2a9bf}form .input{min-height:60px}form .input{margin-bottom:30px}.header-search{display:block;text-align:center;background:rgba(0,0,0,.9);opacity:0;visibility:hidden;position:fixed;top:0;left:0;width:100%;height:100%;-webkit-transition:all .3s ease-in-out;transition:all .3s ease-in-out;z-index:999;cursor:pointer}.search-form{width:100%;position:absolute;top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}.search-form .input-search{background-color:#fff;color:#4b4b4b;height:auto;width:100%;font-size:18px;line-height:1;border:none;margin:0 auto;padding:20px 50px 20px 30px;width:100%}.search-group{position:relative;max-width:500px;margin:0 auto;width:80%}.search-submit{position:absolute;right:0;top:3px;background:0 0;border:0;font-size:24px;bottom:0;padding:0;right:15px;height:100%;color:#ff4157}.search-form .input-search{padding:30px}.search-submit{right:25px}.search-group{max-width:800px;width:90%}.image-block{position:relative;width:100%}.image-block img{width:60%}.image-block img{width:50%}.image-block img{width:100%}

}

	</style>

	<script async src="https://www.googletagmanager.com/gtag/js?id=G-T0S9WRC494"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'G-T0S9WRC494');
	</script>
	
	
</head>
 
<body class="body-wider">

	<noscript id="deferred-styles">
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/all.css') }}"/>

    </noscript>
    <script>
      var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
      };
      var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
          window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
      if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
      else window.addEventListener('load', loadDeferredStyles);
    </script>
	<!-- Header -->
	<header class="is-sticky is-shrink is-boxed header-s1" id="header">
		<div class="header-box">
			<div class="header-main">
				<div class="header-wrap">
					<!-- Logo  -->
					<div class="header-logo logo">
						<a href="./" class="logo-link">
							<img class="logo-white" src="https://webnolic-dev.s3.amazonaws.com/images/1569948830_logo-header.png" srcset="https://webnolic-dev.s3.amazonaws.com/images/1569948830_logo-header.png" alt="logo">
						</a>
					</div>

					<!-- Menu Toogle -->
					<div class="header-nav-toggle">
						<!--<a href="#" class="search search-mobile search-trigger"><i class="icon ti-search"></i></a>-->
						<a href="#" class="navbar-toggle" data-menu-toggle="header-menu">
							<div class="toggle-line">
								<span></span>
							</div>
						</a>
					</div>
					<!-- Menu Toogle -->

					<!-- Menu -->
					<div class="header-navbar">
						<div class="header-top">
							<ul style="display: flex;justify-content:flex-end;" class="header-contact-list">
	                            <li>
	                                <img src="{{ asset('images/usa16.png') }}" alt="phone" />
	                                <a style="font-size: 13px;color:#151212" href="tel:+1 831 298 5802">+1 831 298 5802</a>
	                            </li>
	                        </ul>
						</div>
						<nav class="header-menu" id="header-menu">
							<ul class="menu">
								<li class="menu-item">
									<a class="menu-link nav-link {{ (Request::is('/') ? 'active' : '') }}" href="{{ url('/') }}">Home</a>
								</li>	
								<li class="menu-item"><a class="menu-link nav-link {{ (Request::is('about') ? 'active' : '') }}" href="{{ url('about') }}">About</a></li>
								<li class="menu-item"><a class="menu-link nav-link {{ (Request::is('contact') ? 'active' : '') }}" href="{{ url('contact') }}">Contact</a></li>
								<li class="menu-item"><a class="menu-link nav-link {{ (Request::is('portfolio') ? 'active' : '') }}" href="{{ url('portfolio') }}">Portfolio</a></li>
								<!--<li class="menu-item"><a class="menu-link nav-link" href="{{ url('blogs') }}">Blog</a></li>-->
								<li class="menu-item"><a class="menu-link nav-link {{ (Request::is('service') ? 'active' : '') }}" href="{{ url('service') }}">What We Do</a></li>
							</ul>
							<ul class="menu-btns">
								<!--<li><a href="" class="btn search search-trigger"><i class="icon ti-search "></i></a></li>-->
								<li><a href="{{ url('contact') }}" class="btn btn-sm">Start A project</a></li>
							</ul>
						</nav>
					</div><!-- .header-navbar -->   

					<!-- header-search -->
					<div class="header-search">
						<form role="search" method="POST" class="search-form" action="#">
							<div class="search-group">
								<input type="text" class="input-search" placeholder="Search ...">
								<button class="search-submit" type="submit"><i class="icon ti-search"></i></button>
							</div>
						</form>
					</div>
					<!-- . header-search -->
				</div>
			</div>
			@yield('banner')
			<!-- .banner -->  
		</div>
	</header>
	<!-- end header -->



    @yield('content')

<!-- section / cta -->
    <div class="section section-cta bg-primary tc-light">
        <div class="container">
            <div class="row gutter-vr-30px align-items-center justify-content-between">
                <div class="col-lg-8 text-center text-lg-left">
                    <div class="cta-text">
                        <h2>Like what you see? <strong> Let’s work</strong></h2>
                    </div>
                </div>
                <div class="col-lg-4 text-lg-right text-center">
                    <div class="cta-btn">
                        <a href="{{ url('contact') }}" class="btn">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .section-cta -->

<!-- footer -->
	<footer class="section footer bg-dark-alt tc-light footer-s1">
		<div class="container">
			<div class="row gutter-vr-30px">
				<div class="col-lg-3 col-sm-6">
					<div class="wgs">
						<div class="wgs-content">
							<div class="wgs-logo">
								<a href="{{ URL::to('/') }}">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569956470_logo-white2.png" srcset="https://webnolic-dev.s3.amazonaws.com/images/1569956470_logo-white2.png" alt="logo">
								</a>
							</div>
							<p>&copy; 2019. All rights reserved.</p>	
						</div>
					</div><!-- .wgs -->
				</div><!-- .col -->
				<div class="col-lg-3 col-sm-6">
					<div class="wgs">
						<div class="wgs-content">
							<h3 class="wgs-title">Company</h3>
							<ul class="wgs-menu">
								<li><a href="{{ url('about') }}">About us</a></li>
								<li><a href="{{ url('service') }}">Why Webnolic?</a></li>
								<li><a href="{{ url('portfolio') }}">Our Works</a></li>
							</ul>
						</div>
					</div><!-- .wgs -->
				</div><!-- .col -->
				<div class="col-lg-3 col-sm-6">
					<div class="wgs">
						<div class="wgs-content">
							<h3 class="wgs-title">Social Links</h3>
							<ul class="wgs-menu">
								<li><a target="_blank" href="https://www.facebook.com/webnolic/"><i class="ti-facebook mr-2"></i>Facebook</a></li>
								<li><a target="_blank" href="https://twitter.com/webnolic"><i class="ti-twitter mr-2"></i>Twitter</a></li>
								<li><a target="_blank" href="https://in.pinterest.com/webnolic/?autologin=true"><i class="ti-pinterest mr-2 "></i>Pinterest</a></li>
							</ul>
						</div>
					</div><!-- .wgs -->
				</div><!-- .col -->
				<div class="col-lg-3 col-sm-6">
					<div class="wgs">
						<div class="wgs-content">
							<h3 class="wgs-title">Get our staff</h3>
							<form class="genox-form" action="{{ url('subscribe') }}" method="POST">
								<div class="form-results"></div>
								<div class="field-group btn-inline">
									<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
									<input name="s_email" type="email" class="input required" placeholder="Your  Email">
									<input type="text" class="d-none" name="form-anti-honeypot" value="">
									<button type="submit"  class="far fa-paper-plane button"></button>
								</div>
							</form>
						</div>
					</div><!-- .wgs -->
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- .container -->
	</footer>
	<!-- .footer -->
	<style>
   .preloader.preloader-dalas:before, .preloader.preloader-dalas:after { background: #ff4157; }

</style>
	<!-- preloader -->
	<div class="preloader preloader-light preloader-dalas no-split">
		<span class="spinner spinner-alt">
			<img class="spinner-brand" src="{{ asset('images/logo.png') }}" alt="Loading...">
		</span>
	</div>
	
</body>

<script type="text/javascript" defer src="{{ asset('assets/js/all.js') }}"></script>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "@id" : "https://webnolic.com/#website",
  "name": "Webnolic",
  "url": "https://webnolic.com",
  "potentialAction": {
	"@type": "SearchAction",
	"target": "https://webnolic.com/blogs/?s={search_term_string}",
	"query-input": "required name=search_term_string"
  }
}
</script>
</html>