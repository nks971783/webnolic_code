<!DOCTYPE html>
<html lang="en">
<head>
<title>Webnolic Admin Panel</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap-responsive.min.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/admin/css/fullcalendar.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/admin/css/datepicker.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/admin/css/matrix-style.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/admin/css/matrix-media.css') }}" />
<link href="{{ asset('assets/admin/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('assets/admin/css/jquery.gritter.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/admin/css/sweetalert2.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap-wysihtml5.css') }}" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="{{ asset('assets/admin/js/jquery.min.js') }}"></script> 
</head>
<body>

<!--Header-part-->
<div id="header">
  <img style="margin: -20px auto;" src="https://webnolic-dev.s3.amazonaws.com/images/1569956470_logo-white2.png" width="200" alt="logo">
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a href="javascript:void(0);"><i class="icon icon-user"></i>  <span class="text">Welcome {{ Auth::user()->name }}</span></a>
    </li>
    <li class=""><a title="" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
                                                                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
  </ul>
</div>
<!--close-top-serch-->
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="{{ ( Request::is('admin')? 'active' : '') }}"><a href="{{ url('admin') }}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li class="{{ ( Request::is('admin/categories') || Request::is('admin/add-category') || Request::is('admin/edit-category/*') ? 'active' : '') }}"><a href="{{ url('admin/categories') }}"><i class="icon icon-th"></i> <span>Category</span></a> </li>
    <li class="{{ ( Request::is('admin/blogs') || Request::is('admin/add-blog') || Request::is('admin/edit-blog/*') ? 'active' : '') }}"><a href="{{ url('admin/blogs') }} "><i class="icon icon-th-list"></i> <span>Blogs</span></a> </li>
  </ul>
</div>
<!--sidebar-menu-->


        @yield('content')
        


<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>
</div>

<!--end-Footer-part-->
<style type="text/css">
  .wysihtml5-sandbox {
    width: 643px !important;
  }
  .error{
    color: red;
  }
</style>
<script src="{{ asset('assets/admin/js/excanvas.min.js') }}"></script> 

<script src="{{ asset('assets/admin/js/jquery.ui.custom.js') }}"></script> 
<script src="{{ asset('assets/admin/js/bootstrap.min.js') }}"></script> 
<script src="{{ asset('assets/admin/js/bootstrap-datepicker.js') }}"></script>
<!--<script src="{{ asset('assets/admin/js/jquery.flot.min.js') }}"></script> -->
<!--<script src="{{ asset('assets/admin/js/jquery.flot.resize.min.js') }}"></script> -->
<script src="{{ asset('assets/admin/js/jquery.peity.min.js') }}"></script> 
<script src="{{ asset('assets/admin/js/fullcalendar.min.js') }}"></script> 
<!--<script src="{{ asset('assets/admin/js/matrix.js') }}"></script> 
<script src="{{ asset('assets/admin/js/matrix.dashboard.js') }}"></script> -->
<script src="{{ asset('assets/admin/js/jquery.gritter.min.js') }}"></script> 
<!--<script src="{{ asset('assets/admin/js/matrix.interface.js') }}"></script> 
<script src="{{ asset('assets/admin/js/matrix.chat.js') }}"></script> -->
<script src="{{ asset('assets/admin/js/jquery.validate.js') }}"></script> 
<!--<script src="{{ asset('assets/admin/js/matrix.form_validation.js') }}"></script> -->
<script src="{{ asset('assets/admin/js/jquery.wizard.js') }}"></script> 
<script src="{{ asset('assets/admin/js/jquery.uniform.js') }}"></script> 
<script src="{{ asset('assets/admin/js/select2.min.js') }}"></script> 
<!--<script src="{{ asset('assets/admin/js/matrix.popover.js') }}"></script> -->
<script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script> 
<!--<script src="{{ asset('assets/admin/js/matrix.tables.js') }}"></script> -->
<script src="{{ asset('assets/admin/js/sweetalert2.js') }}"></script> 

<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:

  $(document).ready(function() {
  $('#summernote').summernote({
  toolbar: [
  ['style', ['style']],
  ['font', ['bold', 'underline', 'clear']],
  ['fontname', ['fontname']],
  ['color', ['color']],
  ['para', ['ul', 'ol', 'paragraph']],
  ['table', ['table']],
  ['insert', ['link', 'picture', 'video']],
  ['view', ['fullscreen', 'codeview']],
],
});

  $('.modal').remove()
  });

 

  function showPopup(link_url, id, tableId){
    let row_count = document.getElementById(tableId).getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
          swal({
          title: 'Are you sure?',
          text: "You want to delete this data!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
           
          if (result.value) {
            $.ajax({url: link_url, success: function(data){
                //console.log(data);
                if(data.status == 1){
                    $('#row_id'+id).remove();
                    if(row_count == 1){
                        $('#'+tableId).find('tbody').append( '<tr><td colspan="7">No Result Found.</td></tr>' );
                    }
                    swal(
                      'Deleted!',
                      'Your data has been deleted.',
                      'success'
                    )
                }else{
                    swal ( "Oops" ,  "Something went wrong, Please try later!" ,  "error" )
                    return;
                }
            }});
          }
         
       
        })
}
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}



</script>
</body>
</html>
