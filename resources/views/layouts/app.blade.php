<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Webnolic') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
 <style>
     
     .container {

    width: 100%;
    margin: 0 auto !important;
    max-width: 1200px;
    display: flex;
    justify-content: center;
    padding-top: 70px;

}
    
.card {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.4);
  transition: 0.3s;
  border-radius: 5px;/
  padding: 40px 20px;
}

.card-body {
    padding: 30px 50px;
}

.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.6);
}

input[type=text], input[type=password],  input[type=email] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}
.card-header{
    text-align: center;
    font-size: 24px;
    color: blue;
    font-weight: 700;
}

.font-check{

    display: flex;
    justify-content: end;
    font-size: 12px;

}

.form-group.row.mb-0:last-child .col-md-8{
    display: flex;
    flex-direction: column;
    }

.form-group.row.mb-0:last-child .col-md-8 .btn.btn-link{
    font-size: 14px;

margin: 0 auto;

padding: 20px 0px;
}

button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}


 </style>



</head>
<body>
    <div id="app">

        <main class="py-4">
            @yield('content')
        </main>
    </div>

   
</body>
</html>
