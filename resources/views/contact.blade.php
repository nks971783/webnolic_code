@extends('layouts/master')

@section('banner')
<div class="banner banner-inner tc-light">
                <div class="banner-block">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="banner-content">
                                    <h1 class="banner-heading">Lets’s Work Together</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-image">
                        <img src="https://webnolic-dev.s3.amazonaws.com/images/1569612384_contact.jpg" alt="banner">
                    </div>
                </div>
                
            </div>
            <!-- .banner -->
@endsection

@section('content')

<div class="secdtion section-x tc-grey-alt">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-8">
                    <div class="section-head">
                        <h5 class="heading-xs dash">Feel the form</h5>
                        <h2>Describe your project and leave us your contact info</h2>
                    </div>
                </div><!-- .col -->
            </div><!-- .row -->
            <div class="row gutter-vr-30px">
                <div class="col-lg-4 order-lg-last">
                    <div class="contact-text contact-text-s2 bg-secondary box-pad ">
                        <div class="text-box">
                            <h4>Webnolic Branch Office</h4>
                            <p class="lead">A-92, Sector 63, Noida Uttar Pradesh India</p>
                        </div>
                        <ul class="contact-list">
                            <li>
                                <em class="contact-icon ti-mobile"></em>
                                <div class="conatct-content">
                                    <a href="tel:+1 831 298 5802">+1 831 298 5802</a>
                                </div>
                            </li>
                            <li>
                                <em class="contact-icon ti-email"></em>
                                <div class="conatct-content">
                                    <a href="mailto:hello@webnolic.com">hello@webnolic.com</a>
                                </div>
                            </li>
                            <li>
                                <em class="contact-icon ti-map-alt"></em>
                                <div class="conatct-content">
                                    <a target="_blank" href="https://www.google.com/maps/place/92,+Sector+63+Rd,+A+Block,+Sector+63,+Noida,+Uttar+Pradesh+201307/@28.6308309,77.3799339,17z/data=!3m1!4b1!4m5!3m4!1s0x390ceff9b0a893f9:0xf2b06e2602d656c2!8m2!3d28.6308309!4d77.3821226">Get Directions</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- .col -->
                <div class="col-lg-8">
                    <form class="genox-form mt-10" action="{{ url('submit-contact') }}" method="POST">
                        <div class="form-results"></div>
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <div class="row">
                            <div class="form-field col-md-6">
                                <input name="cf_name" type="text" placeholder="Your Name" class="input bdr-b required">
                            </div>
                            <div class="form-field col-md-6">
                                <input name="cf_email" type="email" placeholder="Your Email" class="input bdr-b required">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-field col-md-6">
                                <input name="cf_address" type="text" placeholder="Your Address" class="input bdr-b required">
                            </div>
                            <div class="form-field col-md-6">
                                <input name="cf_company" type="text" placeholder="Your Compnay" class="input bdr-b required">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-field col-md-6">
                                <input type="text" name="cf_date" class="input bdr-b datepicker" placeholder="When do you want start?">
                            </div>
                            <div class="form-field form-select col-md-6">
                                <select name="cf_budget" class="form-control input-select bdr-b input required" id="selectid_b">
                                    <option>What your Budget</option>
                                    <option>$100 - $200</option>
                                    <option>$200 - $300</option>
                                    <option>$300 - $400</option>
                                    <option>$400 - $500</option>
                                    <option>$500 < </option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-field col-md-12">
                                <textarea name="cf_msg" placeholder="Briefly tell us about your project. " class="input input-msg bdr-b required"></textarea>
                                <input type="text" class="d-none" name="form-anti-honeypot" value="">
                                <button type="submit" class="btn">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div><!-- .col -->
            </div>
        </div><!-- .container -->
    </div>
    <!-- .section -->

@endsection