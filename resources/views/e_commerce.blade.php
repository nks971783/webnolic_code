@extends('layouts/master')

@section('banner')

<div class="banner banner-inner tc-light">
					<div class="banner-block">
						<div class="container">
							<div class="row">
								<div class="col-xl-6">
									<div class="banner-content">
										<h1 class="banner-heading">eCommerce Development</h1>
										<a href="{{ asset('contact') }}" class="btn">Let’s Talk !</a>
									</div>
								</div>
							</div>
						</div>
						<div class="bg-image">
							<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612399_web-application.jpeg" alt="banner">
						</div>
					</div>
</div>
@endsection

@section('content')
<div class="section section-x tc-grey" style="padding-bottom: 100px;">
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-md-5">
						<div class="section-head section-lg res-m-btm">
							<div class="text-block">
								<h5 class="heading-xs dash">Ecommerce Website Development</h5>
								<h2>We work with multiple frameworks such as Shopify, Woo-commerce, Magento, Big-commerce.</h2>
							</div>
						</div>
					</div><!-- .col -->
					<div class="col-md-6">
						<div class="section-head section-lg">
							<div class="text-box">
								<p class="lead">Looking to create mobile friendly ecommerce designs with robust features like content management, product management, email marketing integration, easy order fulfilment and reporting, look no further. Contact us today!</p>
							</div>
						</div>
					</div><!-- .col -->
		</div><!-- .row --> 
	</div><!-- .container -->
</div>	
<div class="section section-x" style="padding-top: 0;">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<div class="section-head section-sm mtm-10">
							<h2>Our Expertise</h2>
						</div>
					</div>
				</div>
				<!-- .row -->
				<div class="row justify-content-center our-expertise">
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612389_e_wordpress.png" alt="wordpress">
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612388_e_magento.png" alt="magento">
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612387_e_big_commerce.png" alt="big_commerce">
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612388_e_shopify_plus.png" alt="shopify_plus">
							</div>
					</div>
				</div><!-- .row -->

				<div class="row justify-content-center our-expertise">
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612388_e_shopify.png" alt="sopify">
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612387_e_drupal.png" alt="drupal">
							</div>
					</div>
				</div><!-- .row -->

			</div><!-- .container -->
		</div>
		<!-- .section -->	
@endsection