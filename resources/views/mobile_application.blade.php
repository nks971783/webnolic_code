@extends('layouts/master')

@section('banner')

<div class="banner banner-inner tc-light">
					<div class="banner-block">
						<div class="container">
							<div class="row">
								<div class="col-xl-6">
									<div class="banner-content">
										<h1 class="banner-heading">Mobile Development</h1>
										<a href="{{ url('contact') }}" class="btn">Let’s Talk !</a>
									</div>
								</div>
							</div>
						</div>
						<div class="bg-image">
							<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612393_mobile-application.jpeg" alt="banner">
						</div>
					</div>
					
				</div>
@endsection

@section('content')
<div class="section section-x tc-grey">
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-md-5">
						<div class="section-head section-lg res-m-btm">
							<div class="text-block">
								<h5 class="heading-xs dash">Mobile Development</h5>
								<h2>Build, Enhance or Evolve Your Mobile Solution</h2>
							</div>
						</div>
					</div><!-- .col -->
					<div class="col-md-6">
						<div class="section-head section-lg">
							<div class="text-box">
								<p class="lead">Leaders in every way, our team adopts early and learns fast. With experience in the range of development from pure native to cross platform apps, we can help you select the best choice for your project requirements.</p>
							</div>
						</div>
					</div><!-- .col -->
				</div><!-- .row -->

				<!--<div class="row">
					<div class="col-12">
						<div class="image-block block-pad-b-100">
							<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612384_contact.jpg" alt="banner13">
						</div>
					</div>
				</div>-->

				<div class="row justify-content-center gutter-vr-30px">
					<div class="col-12 col-md-4">
						<div class="text-box">
							<h4 class="fw-6">MOBILE FIRST</h4>
							<p>Engage recognized experts in mobile app development to build, test and release your five-star mobile application.</p>
						</div>
					</div><!-- .col -->
					<div class="col-12 col-md-4">
						<div class="text-box">
							<h4 class="fw-6">WEB TO MOBILE</h4>
							<p>Extend the reach of the existing web portals and services with content-rich and transactions-centric mobile apps.</p>
						</div>
					</div><!-- .col -->
					<div class="col-12 col-md-4">
						<div class="text-box">
							<h4 class="fw-6">ENTERPRISE MOBILITY</h4>
							<p>Get a first-class B2B, B2E, or B2C enterprise mobility solution securely interfacing with any type of corporate systems.</p>
						</div>
					</div><!-- .col -->
				</div>
				
			</div><!-- .container -->
		</div>

		<!-- section -->
		<div class="section section-x" style="padding-top: 0;">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<div class="section-head section-sm mtm-10">
							<h2>Tools & Technologies</h2>
						</div>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612380_40-Mobile-IOS-300x164.png" width="100" alt="ios">
									<p>IOS</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612380_41-Mobile-Android-300x164.png" width="100" alt="android">
									<p>ANDROID</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612380_44-Mobile-Ionic.png" width="100" alt="ionic">
									<p>IONIC</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612374_17-Web-React.png" width="100" alt="react_native">
									<p>REACT NATIVE</p>
							</div>
					</div>
				</div>
			</div><!-- .container -->
		</div>
		<!-- .section -->
@endsection