@extends('layouts/master')

@section('banner')

<div class="banner banner-inner tc-light">
					<div class="banner-block">
						<div class="container">
							<div class="row">
								<div class="col-xl-6">
									<div class="banner-content">
										<h1 class="banner-heading">Cloud Services</h1>
										<a href="{{ asset('contact') }}" class="btn">Let’s Talk !</a>
									</div>
								</div>
							</div>
						</div>
						<div class="bg-image">
							<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612399_web-application.jpeg" alt="banner">
						</div>
					</div>
</div>
@endsection

@section('content')
<div class="section section-x tc-grey" style="padding-bottom: 0;">
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-md-5">
						<div class="section-head section-lg res-m-btm">
							<div class="text-block">
								<h5 class="heading-xs dash">Cloud Services</h5>
								<h2>Cloud services refer to any IT services that are provisioned and accessed from a cloud computing provider.</h2>
							</div>
						</div>
					</div><!-- .col -->
					<div class="col-md-6">
						<div class="section-head section-lg">
							<div class="text-box">
								<p class="lead">A cloud service is any service made available to users on demand via the Internet from a cloud computing provider's servers as opposed to being provided from a company's own on-premises servers.</p>
							</div>
						</div>
					</div><!-- .col -->
		</div><!-- .row --> 
	</div><!-- .container -->
</div>	
<div class="section section-x" style="padding-top: 0;">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<div class="section-head section-sm mtm-10">
							<h2>Our Expertise</h2>
						</div>
					</div>
				</div>
				<!-- .row -->
				<div class="row justify-content-center our-expertise">
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612383_aws.png" width="100" alt="aws">
								<p>AMAZON WEB SERVICES</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612389_gcp.png" width="100" alt="gcp">
								<p>GOOGLE CLOUD PLATFORM</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612393_ibm_cloud.jpg" width="100" alt="cloud">
								<p>IBM CLOUD</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612383_azure.png" width="100" alt="azure">
								<p>MICROSOFT AZURE</p>
							</div>
					</div>
				</div><!-- .row -->

				<div class="row justify-content-center our-expertise">
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612397_salesforce.png" width="100" alt="salesforce">
								<p>SALESFORCE</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612396_rackspace.png" width="100" alt="rackspace">
								<p>RACKSPACE</p>
							</div>
					</div>
				</div><!-- .row -->

			</div><!-- .container -->
		</div>
		<!-- .section -->		
@endsection