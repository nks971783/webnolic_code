@extends('layouts/master')

@section('banner')

<div class="banner banner-inner tc-light">
				<div class="banner-block">
					<div class="container">
						<div class="row">
							<div class="col-xl-6">
								<div class="banner-content">
									<h1 class="banner-heading">We Specialize in Digital Solutions</h1>
									<a href="{{ url('contact') }}" class="btn">Let’s Talk !</a>
								</div>
							</div>
						</div>
					</div>
					<div class="bg-image">
						<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612397_services.jpg" alt="banner">
					</div>
				</div>
				
			</div>
			<!-- .banner --> 

@endsection

@section('content')
<div class="section section-x tc-grey-alt">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-md-5">
					<div class="section-head section-md res-m-btm">
						<h5 class="heading-xs dash">OUR SERVICES</h5>
						<h2>We help brands and companies stand out in the digital age.</h2>
					</div>
				</div><!-- .col -->
				<div class="col-md-6">
					<div class="section-head section-md">
						<div class="text-box">
							<p class="lead">Your business’s success has hinged on your team’s hard work and in-depth knowledge. Now, you’re ready to get more from your marketing. </p>
						</div>
					</div>
				</div><!-- .col -->
			</div><!-- .row -->
			<div class="row justify-content-center gutter-vr-30px">
				<div class="col-sm-6 col-xl-3">
					<div class="feature feature-s2 feature-s2-inner bdr">
						<div class="feature-icon">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1570012038_dm-icon.png" width="80" height="80" alt="icon">
						</div>
						<div class="feature-content-s3">
							<h3>Digital Marketing</h3>
							<p>Prospects are people too. Marketing should be scientific, but never robotic.</p>
							<a href="{{ url('digital-marketing') }}" class="btn btn-arrow">Read More</a>
						</div>
					</div>
				</div><!-- .col -->
				<div class="col-sm-6 col-xl-3">
					<div class="feature feature-s2 feature-s2-inner bdr">
						<div class="feature-icon">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1570012039_wd-icon.png" width="80" height="80" alt="icon">
						</div>
						<div class="feature-content-s3">
							<h3>Website Design</h3>
							<p>Our visual designers and UX specialists work together to create elegant, useful, unique solutions.</p>
							<a href="{{ url('website-design') }}" class="btn btn-arrow">Read More</a>
						</div>
					</div>
				</div><!-- .col -->
				<div class="col-sm-6 col-xl-3">
					<div class="feature feature-s2 feature-s2-inner bdr">
						<div class="feature-icon">
							<img src="https://webnolic-dev.s3.amazonaws.com/images/1570012039_wad-icon.png" width="80" height="80" alt="icon">
						</div>
						<div class="feature-content-s3">
							<h3>Website Development</h3>
							<p>Productive & engaging web solutions for smarter work and improved customer service.</p>
							<a href="{{ url('website-development') }}" class="btn btn-arrow">Read More</a>
						</div>
					</div>
				</div><!-- .col -->
				<div class="col-sm-6 col-xl-3">
					<div class="feature feature-s2 feature-s2-inner bdr">
						<div class="feature-icon">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1570012038_md-icon.png" width="80" height="80" alt="icon">
						</div>
						<div class="feature-content-s3">
							<h3>Mobile Application</h3>
							<p>Native, hybrid, and cross-platform mobile apps for consumer-facing and corporate environments.</p>
							<a href="{{ url('mobile-application') }}" class="btn btn-arrow">Read More</a>
						</div>
					</div>
				</div><!-- .col -->
			</div><!-- .row-->

			<div class="row justify-content-center gutter-vr-30px">
				<div class="col-sm-6 col-xl-4">
					<div class="feature feature-s2 feature-s2-inner bdr">
						<div class="feature-icon">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1570012037_cloud.png" width="80" height="80" alt="icon">
						</div>
						<div class="feature-content-s3">
							<h3>Cloud Services</h3>
							<p>The cloud offers many incredible advantages, which can definitely create a prolonged change in the web application development lifecycle</p>
							<a href="{{ url('cloud-services') }}" class="btn btn-arrow">Read More</a>
						</div>
					</div>
				</div><!-- .col -->
				<div class="col-sm-6 col-xl-4">
					<div class="feature feature-s2 feature-s2-inner bdr">
						<div class="feature-icon">
							<img src="https://webnolic-dev.s3.amazonaws.com/images/1570012038_e-commerce.png" width="80" height="80" alt="icon">
						</div>
						<div class="feature-content-s3">
							<h3>eCommerce</h3>
							<p>Increase clarity and wealth of information, which leads to a better user experience and improves conversion rates.</p>
							<a href="{{ url('e-commerce') }}" class="btn btn-arrow">Read More</a>
						</div>
					</div>
				</div><!-- .col -->
				<div class="col-sm-6 col-xl-4">
					<div class="feature feature-s2 feature-s2-inner bdr">
						<div class="feature-icon">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1570012035_ai_ml.png" width="80" height="80" alt="icon">
						</div>
						<div class="feature-content-s3">
							<h3>AI & Machine Learning</h3>
							<p>Artificial Intelligence is the machines which are designed and programmed in such a manner that they and think and act like a human.</p>
							<a href="{{ url('machine-ai') }}" class="btn btn-arrow">Read More</a>
						</div>
					</div>
				</div>
			</div><!-- .row-->

		</div><!-- .container -->
	</div>
	<!-- .section -->

	<!-- section -->
	<div class="section section-x">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-7 text-center">
					<div class="section-head section-md tc-light">
						<h5 class="heading-xs">Our Process</h5>
						<h2>We know the best results come from bold ideas. We enjoy what we do and we want you to too.</h2>
					</div>
				</div>
			</div>
			<div class="row justify-content-center gutter-vr-30px">
				<div class="col-lg-4 col-md-6 text-center">
					<div class="feature feature-s2 feature-s2-alt-inner bg-light">
						<div class="icon-box">
							<strong class="icon">01</strong>
							<div class="icon-box-content">
								<h6 class="tc-primary">Concept</h6>
							</div>
						</div>
						<div class="feature-content">
							<p>We listen, research, ideate, marinate, present, and launch Duis aute irure dolor in voluptate.</p>
						</div>
					</div>
				</div><!-- .col -->
				<div class="col-lg-4 col-md-6 text-center">
					<div class="feature feature-s2 feature-s2-alt-inner bg-light">
						<div class="icon-box">
							<strong class="icon">02</strong>
							<div class="icon-box-content">
								<h6 class="tc-primary">Production</h6>
							</div>
						</div>
						<div class="feature-content">
							<p>The best crews with the most up-to-date gear and technologies capture your story. </p>
						</div>
					</div>
				</div><!-- .col -->
				<div class="col-lg-4 col-md-6 text-center">
					<div class="feature feature-s2 feature-s2-alt-inner bg-light">
						<div class="icon-box">
							<strong class="icon">03</strong>
							<div class="icon-box-content">
								<h6 class="tc-primary">Post Production</h6>
							</div>
						</div>
						<div class="feature-content">
							<p>Producers, editors, animators, shape and sculpt your video til it’s ready for prime time.</p>
						</div>
					</div>
				</div><!-- .col -->
			</div><!-- .row-->
		</div><!-- .container -->

		<!-- bg -->
		<div class="bg-image bg-fixed overlay-dark-light">
			<img src="{{ asset('images/services_process.jpg') }}" alt="services">
		</div>
		<!-- end bg -->
	</div>
	<!-- .section -->

	<!-- section / logo -->
	<!--<div class="section section-x">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-5 text-center">
					<div class="section-head section-md">
						<h5 class="heading-xs dash dash-both">Clients</h5>
						<h2>Our friends who love to work with us</h2>
					</div>
				</div>
			</div>
			<div class="row justify-content-center gutter-vr-40px">
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-a.png') }}" alt="">
					</div>
				</div>
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-b.png') }}" alt="">
					</div>
				</div>
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-c.png') }}" alt="">
					</div>
				</div>
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-a.png') }}" alt="">
					</div>
				</div>
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-d.png') }}" alt="">
					</div>
				</div>
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-e.png') }}" alt="">
					</div>
				</div>
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-a.png') }}" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>-->
@endsection