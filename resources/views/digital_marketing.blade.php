@extends('layouts/master')

@section('banner')

<div class="banner banner-inner tc-light">
					<div class="banner-block">
						<div class="container">
							<div class="row">
								<div class="col-xl-6">
									<div class="banner-content">
										<h1 class="banner-heading">Digital Media</h1>
										<a href="{{ url('contact') }}" class="btn">Let’s Talk !</a>
									</div>
								</div>
							</div>
						</div>
						<div class="bg-image">
							<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612395_online-marketing.jpeg" alt="banner">
						</div>
					</div>
					
				</div>
@endsection

@section('content')
<div class="section section-x tc-grey">
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-md-5">
						<div class="section-head section-lg res-m-btm">
							<div class="text-block">
								<h5 class="heading-xs dash">Digital Media</h5>
								<h2>Seamless integration across all digital media channels</h2>
							</div>
						</div>
					</div><!-- .col -->
					<div class="col-md-6">
						<div class="section-head section-lg">
							<div class="text-box">
								<p class="lead">It’s not just our track record, the efficiency of our process and the quality of our products. It’s the secret ingredient that makes it all happen.</p>
							</div>
						</div>
					</div><!-- .col -->
				</div><!-- .row -->
				<!--<div class="row">
					<div class="col-12">
						<div class="image-block block-pad-b-100">
							<img src="{{ asset('images/image-full-a.jpg') }}" alt="banner8">
						</div>
					</div>
				</div>-->
				<div class="row justify-content-center gutter-vr-30px">
					<div class="col-12 col-md-4">
						<div class="text-box">
							<h4 class="fw-6">What we offer</h4>
							<p>We work at the frontier of interactive development and design. We are highly skilled and happily take on complex technical challenges in order to fulfill creative dreams We believe having fun leads to better results.</p>
						</div>
					</div><!-- .col -->
					<div class="col-12 col-md-4">
						<div class="text-box">
							<h4 class="fw-6">Market Research</h4>
							<p>We’re always looking to identify genuine problems amongst your consumers and provide an effective solution. By adopting a data-led approach, we start every new campaign, project and client relationship by gathering.</p>
						</div>
					</div><!-- .col -->
					<div class="col-12 col-md-4">
						<div class="text-box">
							<h4 class="fw-6">Competitor Analysis</h4>
							<p>Looking at what your competitors are doing is vital to the success of any new project. Unlike many other agencies, we don’t look to replicate your nearest rivals – we simply identify what we’re missing to fill in any gaps.</p>
						</div>
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div>
		<!-- .section -->

		<!-- section -->
		<div class="section section-x">
			<div class="container">
				<div class="row justify-content-end">
					<div class="col-lg-6">
						<div class="text-block service-block bg-light gutter-vr-30px">
							<div class="text-box">
								<h4 class="fw-6">Plan</h4>
								<p>We measure success and report back to you in various ways. Depending on your personal preferences.</p>
							</div>
							<div class="text-box">
								<h4 class="fw-6">Actions</h4>
								<ul class="list">
									<li>Workshops</li>
									<li>User journey mapping</li>
									<li>Customer assessments</li>
									<li>Analytics / statistical assessment</li>
								</ul>
							</div>
							<div class="text-box">
								<h4 class="fw-6">Outcomes</h4>
								<ul class="list">
									<li>Highly engaged user reviews</li>
									<li>Collection of Customer feedback</li>
									<li>Customer assessments</li>
									<li>Reactive to market changes.</li>
								</ul>
							</div>
						</div>
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .container -->
			<!-- bg -->
			<div class="bg-image bg-fixed">
				<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612406_whatWeAre.jpg" alt="whatWeAre">
			</div>
			<!-- end bg -->
		</div>
		<!-- .section -->

		<!-- section -->
		<div class="section section-x">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<div class="section-head section-sm mtm-10">
							<h2>Recent Work</h2>
						</div>
					</div>
				</div>
				<!-- .row -->
				<div class="row justify-content-center gutter-vr-30px">
					<div class="col-10 col-md-4">
						<a href="dallas-work-single.html">
							<div class="project-item">
								<div class="project-image">
									<img src="{{ asset('images/project-l.jpg') }}" alt="project-1">
								</div>
								<div class="project-over">
									<div class="project-content">
										<h4>Tinder Online</h4>
										<p>Deigital Media</p>
									</div>	
								</div>
							</div>
						</a>
					</div><!-- .col -->
					<div class="col-10 col-md-4">
						<a href="dallas-work-single.html">
							<div class="project-item">
								<div class="project-image">
									<img src="{{ asset('images/project-m.jpg') }}" alt="project-2">
								</div>
								<div class="project-over">
									<div class="project-content">
										<h4>Tinder Online</h4>
										<p>Deigital Media</p>
									</div>	
								</div>
							</div>
						</a>
					</div><!-- .col -->
					<div class="col-10 col-md-4">
						<a href="dallas-work-single.html">
							<div class="project-item">
								<div class="project-image">
									<img src="{{ asset('images/project-n.jpg') }}" alt="project-3">
								</div>
								<div class="project-over">
									<div class="project-content">
										<h4>Tinder Online</h4>
										<p>Deigital Media</p>
									</div>	
								</div>
							</div>
						</a>
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div>
		<!-- .section -->
@endsection