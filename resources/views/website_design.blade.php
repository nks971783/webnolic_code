@extends('layouts/master')

@section('banner')

<div class="banner banner-inner tc-light">
					<div class="banner-block">
						<div class="container">
							<div class="row">
								<div class="col-xl-6">
									<div class="banner-content">
										<h1 class="banner-heading">Website Design</h1>
										<a href="{{ url('contact') }}" class="btn">Let’s Talk !</a>
									</div>
								</div>
							</div>
						</div>
						<div class="bg-image">
							<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612406_website-design.jpeg" alt="banner">
						</div>
					</div>
					
				</div>
@endsection

@section('content')
<div class="section section-x tc-grey" style="padding-bottom: 0;">
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-md-5">
						<div class="section-head section-lg res-m-btm">
							<div class="text-block">
								<h5 class="heading-xs dash">Website Design</h5>
								<h2>Elevate Your Software Experience</h2>
							</div>
						</div>
					</div><!-- .col -->
					<div class="col-md-6">
						<div class="section-head section-lg">
							<div class="text-box">
								<p class="lead">Able to lead the creation of the user experience or execute it through visual design, we bring expertise in all stages from proof-of-concept prototyping to complete mobile/web solution transformation.</p>
							</div>
						</div>
					</div><!-- .col -->
				</div><!-- .row -->
				
			</div><!-- .container -->
		</div>
		<!-- .section -->

		<!-- section -->
		
		<!-- .section -->

		<!-- section -->
		<div class="section section-x" style="padding-top: 0;">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<div class="section-head section-sm mtm-10">
							<h2>Tools & Technologies</h2>
						</div>
					</div>
				</div>
				<!-- .row -->
				<div class="row justify-content-center">
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612370_01-UX-Sketch.png" width="100" alt="sketch">
									<p>SKETCH</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612372_02-UX-InVision.png" width="100" alt="invision">
									<p>INVISION</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612373_03-UX-Craft.png" width="100" alt="craft">
									<p>CRAFT</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612373_04-UX-Adobe-CC.png" width="100" alt="adobe">
									<p>ADOBE CC</p>
							</div>
					</div>
				</div>

			</div><!-- .container -->
		</div>
		<!-- .section -->
@endsection