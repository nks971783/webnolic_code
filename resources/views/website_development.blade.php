@extends('layouts/master')

@section('banner')

<div class="banner banner-inner tc-light">
					<div class="banner-block">
						<div class="container">
							<div class="row">
								<div class="col-xl-6">
									<div class="banner-content">
										<h1 class="banner-heading">Website Development</h1>
										<a href="{{ asset('contact') }}" class="btn">Let’s Talk !</a>
									</div>
								</div>
							</div>
						</div>
						<div class="bg-image">
							<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612399_web-application.jpeg" alt="banner">
						</div>
					</div>
</div>
@endsection

@section('content')
<div class="section section-x tc-grey">
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-md-5">
						<div class="section-head section-lg res-m-btm">
							<div class="text-block">
								<h5 class="heading-xs dash">Website Development</h5>
								<h2>Productive & engaging web solutions for smarter work and improved customer service.</h2>
							</div>
						</div>
					</div><!-- .col -->
					<div class="col-md-6">
						<div class="section-head section-lg">
							<div class="text-box">
								<p class="lead">Custom web development offers options for businesses who seek to provide a flexible, consistent experience for users across platforms. Leveraging new approaches to web development including progressive web apps, we bring front-end, back-end and architecture ability together to deliver on your business need and maximize delivery speed.</p>
							</div>
						</div>
					</div><!-- .col -->
				</div><!-- .row -->
				
				<div class="row justify-content-center gutter-vr-30px">
					<div class="col-12 tools">
						<h3>BUILD THE RIGHT SOLUTIONS WITH THE RIGHT TECHNOLOGIES AND TOOLS</h3>
					</div>
					<div class="col-12 col-md-4">
						<div class="text-box">
							<h4 class="fw-6">TECHNOLOGY CONSULTING</h4>
							<p>Our technical experts perform a thorough evaluation of your project needs and available technology stacks to help you pick the most appropriate option.</p>
						</div>
					</div>
					<div class="col-12 col-md-4">
						<div class="text-box">
							<h4 class="fw-6">DISCOVERY AND PLANNING</h4>
							<p>We help define the project strategy and roadmap, clarify requirements and prepare the required documentation to ensure you are fixing the right problems and optimizing the right actions.</p>
						</div>
					</div>
					<div class="col-12 col-md-4">
						<div class="text-box">
							<h4 class="fw-6">SOFTWARE PROTOTYPING</h4>
							<p>A proof-of-concept helps evaluate the feasibility of your concept for real-world implementation and assess the potential of your idea with minimum resources consumed.</p>
						</div>
					</div>
				</div> 
			</div><!-- .container -->
		</div>
		<!-- .section -->

		<!-- <div class="section section-x">
			<div class="container">
				<div class="row justify-content-end">
					<div class="col-lg-6">
						<div class="text-block service-block bg-light gutter-vr-30px">
							<div class="text-box">
								<h4 class="fw-6">Plan</h4>
								<p>We measure success and report back to you in various ways. Depending on your personal preferences.</p>
							</div>
							<div class="text-box">
								<h4 class="fw-6">Actions</h4>
								<ul class="list">
									<li>Workshops</li>
									<li>User journey mapping</li>
									<li>Customer assessments</li>
									<li>Analytics / statistical assessment</li>
								</ul>
							</div>
							<div class="text-box">
								<h4 class="fw-6">Outcomes</h4>
								<ul class="list">
									<li>Highly engaged user reviews</li>
									<li>Collection of Customer feedback</li>
									<li>Customer assessments</li>
									<li>Reactive to market changes.</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-image bg-fixed">
				<img src="{{ asset('images/bg-g.jpg') }}" alt="">
			</div>
		</div> -->

		<!-- section -->
		<div class="section section-x" style="padding-top: 0;">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<div class="section-head section-sm mtm-10">
							<h2>Our Expertise</h2>
						</div>
					</div>
				</div>
				<!-- .row -->
				<div class="row justify-content-center our-expertise">
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612373_16-Web-Angular.png" width="100" alt="angular">
									<p>ANGULAR</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612374_19-Web-HTML5.png" width="100" alt="html5">
									<p>HTML5</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612375_20-Web-CSS3.png" width="100" alt="css3">
									<p>CSS3</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612375_21-Web-jQuery.png" width="100" alt="jquery">
									<p>JQUERY</p>
							</div>
					</div>
				</div><!-- .row -->

				<div class="row justify-content-center our-expertise">
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612375_22-Web-Bootstrap.png" width="100" alt="bootstrap">
									<p>BOOTSTRAP</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612375_23-Web-Firebase-300x164.png" width="100" alt="firebase">
									<p>FIREBASE</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612376_29-Web-PHP.png" width="100" alt="php">
									<p>PHP</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612376_30-Web-NodeJS.png" width="100" alt="node_js">
									<p>NODE.JS</p>
							</div>
					</div>
				</div><!-- .row -->

				<div class="row justify-content-center our-expertise">
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612377_31-Web-Python.png" width="100" alt="python">
									<p>PYTHON</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612377_32-Web-Microsoft-SQL-Server.png" width="100" alt="sql_server">
									<p>MS SQL SERVER</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612378_34-Web-MySQL.png" width="100" alt="mysql">
									<p>MYSQL</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612378_35-Web-PostgresQL.png" width="100" alt="postgresql">
									<p>POSTGRESQL</p>
							</div>
					</div>
				</div><!-- .row -->

				<div class="row justify-content-center our-expertise">
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612379_37-Web-MongoDB-300x164.png" width="100" alt="mongodb">
									<p>MONGODB</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612379_38-Web-Redis.png" width="100" alt="redis">
									<p>REDIS</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612374_17-Web-React.png" width="100" alt="react">
									<p>REACT</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
									<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612381_47-Mobile-SQLite.png" width="100" alt="sqlite">
									<p>SQLITE</p>
							</div>
					</div>
				</div><!-- .row -->

			</div><!-- .container -->
		</div>
		<!-- .section -->		
@endsection