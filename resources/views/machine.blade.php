@extends('layouts/master')

@section('banner')

<div class="banner banner-inner tc-light">
					<div class="banner-block">
						<div class="container">
							<div class="row">
								<div class="col-xl-6">
									<div class="banner-content">
										<h1 class="banner-heading">Machine & AI</h1>
										<a href="{{ asset('contact') }}" class="btn">Let’s Talk !</a>
									</div>
								</div>
							</div>
						</div>
						<div class="bg-image">
							<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612399_web-application.jpeg" alt="banner">
						</div>
					</div>
</div>
@endsection

@section('content')
<div class="section section-x tc-grey" style="padding-bottom: 100px;">
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-md-5">
						<div class="section-head section-lg res-m-btm">
							<div class="text-block">
								<h5 class="heading-xs dash">AI & Machine Learning</h5>
								<h2>Machine Learning is an enabling technology that allows web applications to adapt over time by observing and learning from users' habits, idiosyncrasies, and preferences</h2>
							</div>
						</div>
					</div><!-- .col -->
					<div class="col-md-6">
						<div class="section-head section-lg">
							<div class="text-box">
								<p class="lead">Machine Learning, a branch of Artificial Intelligence, offers another advantage in person-machine interactions. Without learning capabilities, applications will approach a problem in the same way time after time, and make the same mistake without modifying or optimizing the solution based on prior experience.</p>
							</div>
						</div>
					</div><!-- .col -->
		</div><!-- .row --> 
	</div><!-- .container -->
</div>	
<div class="section section-x" style="padding-top: 0;">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<div class="section-head section-sm mtm-10">
							<h2>Our Expertise</h2>
						</div>
					</div>
				</div>
				<!-- .row -->
				<div class="row justify-content-center our-expertise">
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612397_scikit_learn.png" width="100" alt="scikit">
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612393_keras.png" width="100" alt="keras">
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612383_caffe.png" width="100" alt="caffe">
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="https://webnolic-dev.s3.amazonaws.com/images/1569954020_theano.png" width="100" alt="theano">
							</div>
					</div>
				</div><!-- .row -->
				<!--
				<div class="row justify-content-center our-expertise">
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="{{ asset('images/salesforce.png') }}" width="100" alt="banner35">
								<p>SALESFORCE</p>
							</div>
					</div>
					<div class="col-10 col-md-3">
							<div class="project-item">
								<img src="{{ asset('images/rackspace.png') }}" width="100" alt="banner35">
								<p>RACKSPACE</p>
							</div>
					</div>
				</div> -->

			</div><!-- .container -->
		</div>
		<!-- .section -->		
@endsection