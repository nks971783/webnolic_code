@extends('layouts/master')

@section('banner')

<div class="banner banner-inner tc-light">
				<div class="banner-block">
					<div class="container">
						<div class="row">
							<div class="col-sm-10 col-10 col-xl-7">
								<div class="banner-content">
									<h1 class="banner-heading">It’s always about YOU at Webnolic!</h1>
									<a href="{{ url('contact') }}" class="btn">Let’s Talk !</a>
								</div>
							</div>
						</div>
					</div>
					<div class="bg-image">
						<img src="https://webnolic-dev.s3.amazonaws.com/images/1570011227_homeAbout.jpg" alt="banner">
					</div>
				</div>
</div>
			<!-- .banner --> 

@endsection

@section('content')

<!-- section -->
	<div class="section section-x tc-grey-alt">
		<div class="container">
			<div class="row gutter-vr-30px">
				<div class="col-md-6">
					<div class="text-block pr-2rem">
						<h5 class="heading-xs dash t-u">We Are</h5>
						<h2>Webnolic is a bold-thinking digital agency working for leading brands worldwide.</h2>
						<p class="lead">It’s not just our track record, the efficiency of our process and the quality of our products. It’s the secret ingredient that makes it all happen: the bravery of our people.</p>
					</div>
				</div><!-- .col -->
				<div class="col-md-6 tc-grey">
					<div class="text-block">
						<div class="row gutter-vr-20px">
							<div class="col-12 col-md-12 col-lg-6">
								<div class="text-box project-box-pad bg-secondary h-full">
									<h4>What we do</h4>
									<p>We work at the frontier of interactive development and design. We are highly skilled and happily take on complex technical challenges.</p>
								</div>
							</div>
							<div class="col-12 col-md-12 col-lg-6">
								<div class="text-box project-box-pad bg-secondary h-full">
									<h4>Culture</h4>
									<p>A friendly, ambitious team in a great office space makes Uppercase an awesome place to work. We believe having fun leads to better results dadcation.</p>
								</div>
							</div>
						</div>
					</div>
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- .container -->
	</div>
	<!-- .section -->

	<!-- section -->
	<div class="section section-x tc-grey-alt">
		<div class="container">
			<div class="row justify-content-end">
				<div class="col-md-10 col-lg-5">
					<div class="text-block bg-light block-pad-80">
						<h5 class="heading-xs dash">Why us</h5>
						<h2>We love to make a difference by creating digital experiences that simplify and enhance value.</h2>
						<a href="{{ url('service') }}" class="btn">Our Services</a>
					</div>
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- .container -->

		<!-- bg-image -->
		<div class="bg-image bg-fixed">
			<img src="https://webnolic-dev.s3.amazonaws.com/images/1569612381_about.jpg" alt="about">
		</div>
		<!-- end bg -->
	</div>
	<!-- .section -->

	<!-- section -->
	<div class="section section-counter-m">
		<div class="container">
			<div class="row align-items-center gutter-vr-30px justify-content-center">
				<div class="col-md-3 col-sm-6 col-6">
					<div class="counter">
						<div class="counter-icon">
							<em class="icon ti-dropbox"></em>
						</div>
						<div class="counter-content">
							<h2 class="count" data-count="34">34</h2>
							<p class="mb-0">Brands Helped</p>
						</div>
					</div>
				</div><!-- .col -->
				<div class="col-md-3 col-sm-6 col-6">
					<div class="counter">
						<div class="counter-icon">
							<em class="icon ti-basketball"></em>
						</div>
						<div class="counter-content">
							<h2 class="count" data-count="145">+145</h2>
							<p class="mb-0">Ongoing Task</p>
						</div>
					</div>
				</div><!-- .col -->
				<div class="col-md-3 col-sm-6 col-6">
					<div class="counter">
						<div class="counter-icon">
							<em class="icon ti-pencil-alt"></em>
						</div>
						<div class="counter-content">
							<h2 class="count" data-count="437">+437</h2>
							<p class="mb-0">Projects Done</p>
						</div>
					</div>
				</div><!-- .col -->
				<div class="col-md-3 col-sm-6 col-6">
					<div class="counter">
						<div class="counter-icon">
							<em class="icon ti-user"></em>
						</div>
						<div class="counter-content">
							<h2 class="count" data-count="375">+375</h2>
							<p class="mb-0">Satisfied Clients</p>
						</div>
					</div>
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- .container -->
	</div>
	<!-- .section -->

	<!-- team -->
	<!--<div class="section section-x team bg-secondary tc-grey-alt">
		<div class="container">
			<div class="row justify-content-center justify-content-sm-start gutter-vr-30px">
				<div class="col-lg-9 col-xl-3 text-center text-sm-left pl-sm-0">
					<div class="section-head section-head-col box-pad-sm">
						<h5 class="heading-xs dash">Meet the team</h5>
						<h2>People of behind work</h2>
						<p class="lead mb-10">We’re always expanding hard working creators.</p>
						<a href="dallas-team.html" class="btn">Meet All Team</a>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3 col-sm-4 col-10">
					<div class="team-single text-center">
						<div class="team-image">
							<img src="{{ asset('images/team-a.jpg') }}" alt="">
						</div>
						<div class="team-content">
							<h5 class="team-name">Andrew rover</h5>
							<p>CEO, Genox</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3 col-sm-4 col-10">
					<div class="team-single text-center">
						<div class="team-image">
							<img src="{{ asset('images/team-b.jpg') }}" alt="">
						</div>
						<div class="team-content">
							<h5 class="team-name">Marina Bedi</h5>
							<p>Developer, Genox</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3 col-sm-4 col-10">
					<div class="team-single text-center">
						<div class="team-image">
							<img src="{{ asset('images/team-c.jpg') }}" alt="">
						</div>
						<div class="team-content">
							<h5 class="team-name">Ajax Holder</h5>
							<p>Head of Research, Genox</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>-->
	<!-- .team -->

	<!-- logo -->
	<!--<div class="section section-x tc-grey-alt">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-4 text-center">
					<div class="section-head section-sm">
						<h5 class="heading-xs dash dash-both">Clients</h5>
						<h2>Our friends who love to work with us</h2>
					</div>
				</div>
			</div>
			<div class="row justify-content-center gutter-vr-30px">
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-a.png') }}" alt="">
					</div>
				</div>
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-b.png') }}" alt="">
					</div>
				</div>
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-c.png') }}" alt="">
					</div>
				</div>
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-d.png') }}" alt="">
					</div>
				</div>
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-b.png') }}" alt="">
					</div>
				</div>
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-e.png') }}" alt="">
					</div>
				</div>
				<div class="col-sm-3 col-4">
					<div class="logo-item">
						<img src="{{ asset('images/client-a.png') }}" alt="">
					</div>
			</div>
			</div>
		</div>
	</div>-->
	<!-- .logo -->

@endsection