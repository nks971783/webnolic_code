const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
		'public/assets/css/vendor.bundle.css',
		'public/assets/css/style.css',
	],'public/assets/css/all.css');

mix.scripts([
	'public/assets/js/jquery.bundle.js',
	'public/assets/js/minify-scripts.js',
	], 'public/assets/js/all.js');