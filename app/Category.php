<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];
    
    protected static function boot()
    {
        parent::boot();

        static::created(function ($blog) {
            $blog->update(['slug' => $blog->name]);
        });
    }
     public function setSlugAttribute($value)
    {
        if (static::whereSlug($slug = Str::slug($value))->exists()) {
            $slug = "{$slug}-{$this->id}";
        }
        $this->attributes['slug'] = $slug;
    }

    public function blogs(){
    	return $this->hasMany('App\Blog');
    }

}
