<?php

namespace App\Http\Controllers;
use App\CurrentPortfolio;

use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function index(){
    	$portfolio = CurrentPortfolio::get();
    	return view('portfolio',compact('portfolio'));
    }
}
