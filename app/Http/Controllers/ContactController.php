<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\SendEmailJob;

class ContactController extends Controller
{
    public function index(){
    	return view('contact');
    }

    public function submitContact(Request $request){
    	$data = (object) null;
    	$data->name = $request->cf_name;
        $data->email = $request->cf_email;
        $data->address = $request->cf_address;
        $data->company = $request->cf_company;
        $data->date = $request->cf_date;
    	$data->budget = $request->cf_budget;
    	$data->message = $request->cf_msg;
    	dispatch(new SendEmailJob($data));
        return json_encode(array('result'=>'success','message'=>'Thank you, We will contact with you shortly.'));
    }

    public function subscribe(Request $request){
        return json_encode(array('result'=>'success','message'=>'<strong>Thank you</strong> for Subscribing.'));
    }
}
