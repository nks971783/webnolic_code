<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Category;

class CategoryController extends Controller
{
    public function categories(){
    	$category = Category::get();
    	return view('admin/category/index',compact('category'));
    }

    public function addCategory(){
    	return view('admin/category/add');
    }

    public function createCategory(Request $request){
    	$this->validate($request,[
		    'name' => 'required',
            'meta_title' => 'required',
            'meta_keyword' => 'required',
            'meta_description' => 'required'
		]);
		Category::create([
            'name' => $request->name,
            'meta_title' => $request->meta_title,
            'meta_keyword' => $request->meta_keyword,
            'meta_description' => $request->meta_description
        ]);
    	return redirect('admin/categories')->with('success','New Category Added.');
    }

    public function editCategory($id){
    	$category = Category::find($id);
    	return view('admin/category/edit',compact('category'));
    }

    public function updateCategory(Request $request,$id){
    	$category = Category::find($id);
    	$category->name = $request->name;
        $category->meta_title = $request->meta_title;
        $category->meta_keyword = $request->meta_keyword;
        $category->meta_description = $request->meta_description;
    	$category->save();
    	return back()->with('success','Category Updated.');
    }

    public function deleteCategory($id){
    	Category::find($id)->delete();
    	return response()->json(['status' => '1', 'message' => 'Success']);
    }


    // public function blogs(){
    //     return $this->hasMany('App\Blog');
    // }
}
