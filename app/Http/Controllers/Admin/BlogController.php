<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBlog;
use App\Http\Requests\UpdateBlog;
use App\{Blog, Category};
use Storage;
use Auth;
use Image;
use File;

class BlogController extends Controller
{
    public function blogs(){
    	$blogs = Blog::paginate(10);
    	return view('admin/blog/index',compact('blogs'));
    }

    public function addBlog(){
    	$categories = Category::get();
    	return view('admin/blog/add',compact('categories'));
    }

    public function createBlog(CreateBlog $request){
        if($request->hasFile('image')){
            $image = time().'.'.$request->image->getClientOriginalExtension();
            $file = $request->file('image');
			$img = Image::make($file);
			$img->interlace();

// save image interlaced
				//$img->save('');
            $filePath = 'blogs/' . $image;
            Storage::disk('s3')->put($filePath, $img->stream(), 'public');
        }
    	Blog::create([
            'title' => $request->title,
            'category_id' => $request->category,
            'description' => $request->description,
            'status' => $request->status,
            'image' => $filePath,
            'user_id' => Auth::user()->id,
            'published_at' => $request->published_at,
            'featured' => $request->featured,
            'meta_title' => $request->meta_title,
            'meta_keyword' => $request->meta_keyword,
            'meta_description' => $request->meta_description
        ]);

        return redirect('admin/blogs')->with('success','New Blog Added.');
    }

    public function editBlog($id){
        $blog = Blog::find($id);
        $categories = Category::get();
        return view('admin/blog/edit',compact('categories','blog'));
    }

    public function updateBlog(UpdateBlog $request,$id){
        $blog = Blog::find($id);
        if($request->hasFile('image')){
            $image = time().'.'.$request->image->getClientOriginalExtension();
            $file = $request->file('image');
            $filePath = 'blogs/' . $image;
            Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
            $old_image = $blog->image;
            $blog->image = $filePath;
        }
        $blog->title = $request->title;
        $blog->category_id = $request->category;
        $blog->description = $request->description;
        $blog->status = $request->status;
        $blog->user_id = Auth::user()->id;
        $blog->published_at = $request->published_at;
        $blog->featured = $request->featured;
        $blog->meta_title = $request->meta_title;
        $blog->meta_keyword = $request->meta_keyword;
        $blog->meta_description = $request->meta_description;
        $blog->save();
        if($request->hasFile('image')){
            $exists = Storage::disk('s3')->exists($old_image);
            if($exists) {
                Storage::disk('s3')->delete($old_image);
            }
        }
        return back()->with('success','Blog Updated.');
    }

    public function deleteBlog($id){
        Blog::find($id)->delete();
        return response()->json(['status' => '1', 'message' => 'Success']);
    }
}
