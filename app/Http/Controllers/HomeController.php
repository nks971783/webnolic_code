<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CurrentPortfolio;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $portfolio = CurrentPortfolio::limit(8)->get();
        //dd($portfolio);
        return view('welcome',compact('portfolio'));
    }
}
