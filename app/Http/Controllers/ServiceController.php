<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function index(){
    	return view('services');
    }

    public function digitalMarketing(){
    	return view('digital_marketing');
    }

    public function websiteDesign(){
    	return view('website_design');
    }

    public function websiteDevelopment(){
    	return view('website_development');
    }

    public function mobileApplication(){
    	return view('mobile_application');
    }

    public function eCommerce(){
        return view('e_commerce');
    }

    public function cloudServices(){
        return view('cloud_services');
    }

    public function machineAi(){
        return view('machine');
    }
}
