<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Storage;

class BlogController extends Controller
{
    public function index(){
    	return view('blog');
    }

    public function blogDetail($slug){
        return view('blog-detail');
    }

    public function imageUpload(){
    	$files = Storage::files('public/images');
        //dd($files);
    	foreach ($files as $key => $value) {
    		$img = Image::make(Storage::get($value))->resize(800, 600);
			$img->interlace();

			$img_file = time().'_'.explode('public/images/',$value)[1];
			$filePath = 'images/' . $img_file;

			Storage::disk('s3')->put($filePath, $img->stream(), 'public');
    	}
    	
        echo "Success!!";
        die;
    }

}
