<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Image;
Use Storage;

class addPortfolio extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:portfolio';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command For Adding Portfolio';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
        $ids = [];
        for($i=0;$i<100;$i++){
            $ids[] = rand(1,6000);
        }

        $portfolio = DB::table('companies')->select('companies.name as company_name','portfolios.*')->join('portfolios', function($join) {
                                                  $join->on('companies.id', '=', 'portfolios.company_id');
                                                })->whereIn('companies.id',$ids)->take(15)->get();
        DB::table('current_portfolios')->delete();
        Storage::disk('s3')->delete('portfolio_images/*');
        foreach($portfolio as $list){
            $url = $list->url;
            if (strpos($url, 'youtube') == false) {
                $img = Image::make($url)->resize(300, 250);
                $img->interlace();

                $img_file = time().'_'.$list->id.'.jpeg';
                $filePath = 'portfolio_images/' . $img_file;

                Storage::disk('s3')->put($filePath, $img->stream(), 'public');
                $img_url = Storage::disk('s3')->url($filePath);
                DB::table('current_portfolios')->insert(['name'=>$list->name,'url'=>$img_url]);
            }
        }
        echo "Success!!";
    }
}
