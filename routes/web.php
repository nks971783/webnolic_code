<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('image-uploader', 'BlogController@imageUpload')->name('image-uploader');

Route::get('/about', 'AboutController@index')->name('about');
Route::get('/service', 'ServiceController@index')->name('service');
Route::get('/portfolio', 'PortfolioController@index')->name('portfolio');
Route::get('/blogs', 'BlogController@index')->name('blogs');
Route::get('blog-detail/{slug}', 'BlogController@blogDetail')->name('blogs');
Route::get('/contact', 'ContactController@index')->name('contact');
Route::post('submit-contact', 'ContactController@submitContact')->name('submit-contact');
Route::post('subscribe', 'ContactController@subscribe')->name('subscribe');

Route::get('digital-marketing', 'ServiceController@digitalMarketing')->name('digital-marketing');
Route::get('website-design', 'ServiceController@websiteDesign')->name('website-design');
Route::get('website-development', 'ServiceController@websiteDevelopment')->name('website-developments');
Route::get('mobile-application', 'ServiceController@mobileApplication')->name('mobile-application');
Route::get('e-commerce', 'ServiceController@eCommerce')->name('e-commerce');
Route::get('cloud-services', 'ServiceController@cloudServices')->name('cloud-services');
Route::get('machine-ai', 'ServiceController@machineAi')->name('machine-ai');

Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);

Route::get('405',['as'=>'405','uses'=>'ErrorHandlerController@errorCode405']);

Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function(){

	Route::get('/','Admin\DashboardController@dashboard');

	//Category

	Route::get('categories','Admin\CategoryController@categories');
	Route::get('add-category','Admin\CategoryController@addCategory');
	Route::post('create-category','Admin\CategoryController@createCategory');
	Route::get('edit-category/{id}','Admin\CategoryController@editCategory');
	Route::post('update-category/{id}','Admin\CategoryController@updateCategory');
	Route::get('delete-category/{id}','Admin\CategoryController@deleteCategory');

	//Blogs

	Route::get('blogs','Admin\BlogController@blogs');
	Route::get('add-blog','Admin\BlogController@addBlog');
	Route::post('create-blog','Admin\BlogController@createBlog');
	Route::get('edit-blog/{id}','Admin\BlogController@editBlog');
	Route::post('update-blog/{id}','Admin\BlogController@updateBlog');
	Route::get('delete-blog/{id}','Admin\BlogController@deleteBlog');

});